package edu.di.unimi.it.sdp.project.client.commands;

import java.util.Map;
import java.util.stream.Collectors;

public class HelpCommand implements ClientCommand {
    private final Map<String, ClientCommand> clientCommands;

    public HelpCommand(Map<String, ClientCommand> commands) {
        this.clientCommands = commands;
    }

    @Override
    public String exec(String... args) {
        return clientCommands.entrySet().stream()
                .map(entry ->
                        entry.getKey() + " - " + entry.getValue())
                .collect(Collectors.joining("\n"));
    }

    @Override
    public String toString() {
        return "displays a list of possible commands";
    }
}
