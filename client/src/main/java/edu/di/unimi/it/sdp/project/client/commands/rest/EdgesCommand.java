package edu.di.unimi.it.sdp.project.client.commands.rest;

import edu.di.unimi.it.sdp.project.client.commands.ClientCommand;
import edu.di.unimi.it.sdp.project.model.Edge;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

public class EdgesCommand implements ClientCommand {
    private final WebTarget target;

    public EdgesCommand(WebTarget target) {
        this.target = target;
    }

    @Override
    public String exec(String... args) {
        try {
            Response response =
                    target.path("edges")
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .get(Response.class);

            List<Edge> edges = response.readEntity(new GenericType<List<Edge>>() {
            });

            return "Edges in the system:\n" +
                    edges.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return "Server isn't responding: is the address correct?";
        }
    }


    @Override
    public String toString() {
        return "displays the edges currently in the grid";
    }
}
