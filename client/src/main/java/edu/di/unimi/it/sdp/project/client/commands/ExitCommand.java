package edu.di.unimi.it.sdp.project.client.commands;

import javax.ws.rs.client.Client;

public class ExitCommand implements ClientCommand {
    private final Client restClient;

    public ExitCommand(Client restClient) {
        this.restClient = restClient;
    }

    @Override
    public String exec(String... args) {
        restClient.close();
        System.exit(0);
        return "Thanks compiler, but application is shutting down";
    }

    @Override
    public String toString() {
        return "quits the application";
    }
}
