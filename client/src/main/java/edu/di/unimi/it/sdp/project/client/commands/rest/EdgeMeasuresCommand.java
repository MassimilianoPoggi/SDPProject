package edu.di.unimi.it.sdp.project.client.commands.rest;

import edu.di.unimi.it.sdp.project.client.commands.ClientCommand;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

public class EdgeMeasuresCommand implements ClientCommand {
    private final WebTarget target;

    public EdgeMeasuresCommand(WebTarget target) {
        this.target = target;
    }

    @Override
    public String exec(String... args) {
        if (args.length < 3)
            return "Not enough arguments: requires edge ID and number of measures";

        try {
            Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            return "Number of measures must be a number";
        }

        try {
            Response response =
                    target.path("stats").path("measures")
                            .path("edge").path(args[1]).path(args[2])
                            .request(MediaType.APPLICATION_JSON_TYPE).get();

            if (response.getStatusInfo().toEnum() == Response.Status.NOT_FOUND)
                return "Invalid edge ID";

            List<ImmutableMeasurement> measurements = response.readEntity(new GenericType<List<ImmutableMeasurement>>() {
            });

            return "Last " + args[2] + " measurements for edge " + args[1] + ":\n" +
                    measurements.stream()
                            .map(Object::toString)
                            .collect(Collectors.joining("\n"));
        } catch (Exception e) {
            return "Server isn't responding: is the address correct?";
        }
    }


    @Override
    public String toString() {
        return "displays the last n measurements for the given edge";
    }
}
