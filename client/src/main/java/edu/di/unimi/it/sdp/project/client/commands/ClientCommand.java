package edu.di.unimi.it.sdp.project.client.commands;

public interface ClientCommand {
    String exec(String... args);
}
