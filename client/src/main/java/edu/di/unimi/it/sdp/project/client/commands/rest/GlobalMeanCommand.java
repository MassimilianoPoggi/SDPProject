package edu.di.unimi.it.sdp.project.client.commands.rest;

import edu.di.unimi.it.sdp.project.client.commands.ClientCommand;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class GlobalMeanCommand implements ClientCommand {
    private final WebTarget target;

    public GlobalMeanCommand(WebTarget target) {
        this.target = target;
    }

    @Override
    public String exec(String... args) {
        if (args.length < 2) {
            return "Not enough arguments: requires number of measures";
        }

        try {
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            return "Number of measures must be a number";
        }

        try {
            Response response =
                    target.path("stats").path("mean")
                            .path(args[1])
                            .request(MediaType.APPLICATION_JSON_TYPE).get();

            double mean = response.readEntity(Double.class);

            return "Mean of the last " + args[1] + " global measurements: " + mean;
        } catch (Exception e) {
            return "Server isn't responding: is the address correct?";
        }
    }


    @Override
    public String toString() {
        return "displays the mean of the last n global measurements";
    }
}
