package edu.di.unimi.it.sdp.project.client;

import edu.di.unimi.it.sdp.project.client.commands.ClientCommand;
import edu.di.unimi.it.sdp.project.client.commands.ExitCommand;
import edu.di.unimi.it.sdp.project.client.commands.HelpCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.EdgeMeanCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.EdgeMeasuresCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.EdgeStdDevCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.EdgesCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.GlobalMeanCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.GlobalMeasuresCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.GlobalStdDevCommand;
import edu.di.unimi.it.sdp.project.client.commands.rest.LocalMeasuresCommand;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class StatisticsClient {
    private static final String PROMPT = "> ";

    public static void main(String... args) {
        if (args.length < 1) {
            System.out.println("Please insert REST server address.");
            return;
        }

        Client restClient = ClientBuilder.newClient();
        String restServer = "http://" + args[0] + "/server/rest";
        WebTarget target = restClient.target(UriBuilder.fromUri(restServer).build());

        Map<String, ClientCommand> commands = getCommands(restClient, target);
        Scanner scanner = new Scanner(new InputStreamReader(System.in));
        while (true) {
            System.out.print(PROMPT);
            String[] lineTokens = scanner.nextLine().split(" ");
            System.out.println(
                    commands.getOrDefault(lineTokens[0],
                            (String... tokens) -> "Command " + tokens[0] + " not found.")
                            .exec(lineTokens));
        }
    }

    private static Map<String, ClientCommand> getCommands(Client restClient, WebTarget target) {
        Map<String, ClientCommand> commands = new HashMap<>();

        commands.put("edges", new EdgesCommand(target));

        commands.put("edgeMeasures", new EdgeMeasuresCommand(target));

        commands.put("localMeasures", new LocalMeasuresCommand(target));
        commands.put("globalMeasures", new GlobalMeasuresCommand(target));

        commands.put("edgeMean", new EdgeMeanCommand(target));
        commands.put("edgeStdDev", new EdgeStdDevCommand(target));

        commands.put("globalMean", new GlobalMeanCommand(target));
        commands.put("globalStdDev", new GlobalStdDevCommand(target));

        commands.put("help", new HelpCommand(commands));
        commands.put("exit", new ExitCommand(restClient));
        return commands;
    }
}
