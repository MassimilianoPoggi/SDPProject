package edu.di.unimi.it.sdp.project.client.commands.rest;

import com.sun.net.httpserver.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class EdgeStdDevCommandTests extends RestCommandTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockStdDevServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testStdDev() {
        EdgeStdDevCommand command = new EdgeStdDevCommand(target);
        assertEquals("Standard deviation of the last 5 measurements for edge edgeID: 10.0",
                command.exec("edgeStdDev", "edgeID", "5"));
    }

    @Test
    public void testNoEdgeFound() {
        EdgeStdDevCommand command = new EdgeStdDevCommand(target);
        assertEquals("Invalid edge ID", command.exec("edgeStdDev", "invalidEdge", "5"));
    }

    @Test
    public void testNotEnoughParameters() {
        EdgeStdDevCommand command = new EdgeStdDevCommand(target);
        assertEquals("Not enough arguments: requires edge ID and number of measures",
                command.exec("edgeStdDev"));
    }

    @Test
    public void testInvalidNumber() {
        EdgeStdDevCommand command = new EdgeStdDevCommand(target);
        assertEquals("Number of measures must be a number",
                command.exec("edgeStdDev", "edgeID", "bad"));
    }

    @Path("/server/rest/stats/")
    public static class MockStdDevServer {

        @GET
        @Path("/stddev/edgeID/5")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getStdDev() {
            return Response.ok(10.0).build();
        }

        @GET
        @Path("/stddev/invalidEdge/5")
        public Response getError() {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}