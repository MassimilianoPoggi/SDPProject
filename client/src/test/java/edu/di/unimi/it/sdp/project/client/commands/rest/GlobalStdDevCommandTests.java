package edu.di.unimi.it.sdp.project.client.commands.rest;

import com.sun.net.httpserver.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class GlobalStdDevCommandTests extends RestCommandTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockStdDevServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testStdDev() {
        GlobalStdDevCommand command = new GlobalStdDevCommand(target);
        assertEquals("Standard deviation of the last 5 global measurements: 10.0",
                command.exec("globalStdDev", "5"));
    }

    @Test
    public void testNotEnoughParameters() {
        GlobalStdDevCommand command = new GlobalStdDevCommand(target);
        assertEquals("Not enough arguments: requires number of measures",
                command.exec("globalStdDev"));
    }

    @Test
    public void testInvalidNumber() {
        GlobalStdDevCommand command = new GlobalStdDevCommand(target);
        assertEquals("Number of measures must be a number",
                command.exec("globalStdDev", "bad"));
    }

    @Path("/server/rest/stats/")
    public static class MockStdDevServer {

        @GET
        @Path("/stddev/5")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getStdDev() {
            return Response.ok(10.0).build();
        }
    }
}