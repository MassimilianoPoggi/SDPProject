package edu.di.unimi.it.sdp.project.client.commands.rest;

import com.sun.net.httpserver.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class GlobalMeanCommandTests extends RestCommandTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockMeanServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testMean() {
        GlobalMeanCommand command = new GlobalMeanCommand(target);
        assertEquals("Mean of the last 5 global measurements: 10.0",
                command.exec("globalMean", "5"));
    }

    @Test
    public void testNotEnoughParameters() {
        GlobalMeanCommand command = new GlobalMeanCommand(target);
        assertEquals("Not enough arguments: requires number of measures",
                command.exec("globalMean"));
    }

    @Test
    public void testInvalidNumber() {
        GlobalMeanCommand command = new GlobalMeanCommand(target);
        assertEquals("Number of measures must be a number",
                command.exec("globalMean", "bad"));
    }

    @Path("/server/rest/stats/")
    public static class MockMeanServer {

        @GET
        @Path("/mean/5")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getMean() {
            return Response.ok(10.0).build();
        }
    }
}