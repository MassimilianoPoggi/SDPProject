package edu.di.unimi.it.sdp.project.client.commands.rest;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EdgesCommandTests extends RestCommandTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockEdgeServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testEdgeList() {
        EdgesCommand command = new EdgesCommand(target);
        assertEquals("Edges in the system:\n" +
                MockEdgeServer.edges.get(0) + "\n" +
                MockEdgeServer.edges.get(1), command.exec("channels"));
    }

    @Path("/server/rest/")
    public static class MockEdgeServer {
        public static List<Edge> edges = Arrays.asList(
                new Edge("edge1", new GridPosition(0, 0), null, 0, 0),
                new Edge("edge2", new GridPosition(50, 50), null, 0, 0)
        );

        @GET
        @Path("/edges")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getEdges() {
            return Response.ok(edges).build();
        }
    }
}
