package edu.di.unimi.it.sdp.project.client.commands;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class HelpCommandTests {
    @Test
    public void testHelpOutput() {
        ClientCommand command = new ClientCommand() {
            @Override
            public String exec(String... args) {
                return null;
            }

            @Override
            public String toString() {
                return "command description";
            }
        };

        Map<String, ClientCommand> commands = new HashMap<>();
        commands.put("commandHandle", command);

        ClientCommand helpCommand = new HelpCommand(commands);

        assertEquals("commandHandle - command description", helpCommand.exec());
    }
}
