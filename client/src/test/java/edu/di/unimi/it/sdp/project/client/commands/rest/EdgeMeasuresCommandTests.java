package edu.di.unimi.it.sdp.project.client.commands.rest;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EdgeMeasuresCommandTests extends RestCommandTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockMeasuresServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testMeasures() {
        EdgeMeasuresCommand command = new EdgeMeasuresCommand(target);
        assertEquals("Last 2 measurements for edge edgeID:\n" +
                        MockMeasuresServer.measurements.get(0) + "\n" +
                        MockMeasuresServer.measurements.get(1),
                command.exec("edgeMeasures", "edgeID", "2"));
    }

    @Test
    public void testNoEdgeFound() {
        EdgeMeasuresCommand command = new EdgeMeasuresCommand(target);
        assertEquals("Invalid edge ID", command.exec("edgeMeasures", "invalidEdge", "2"));
    }

    @Test
    public void testNotEnoughParameters() {
        EdgeMeasuresCommand command = new EdgeMeasuresCommand(target);
        assertEquals("Not enough arguments: requires edge ID and number of measures",
                command.exec("edgeMeasures"));
    }

    @Test
    public void testInvalidNumber() {
        EdgeMeasuresCommand command = new EdgeMeasuresCommand(target);
        assertEquals("Number of measures must be a number",
                command.exec("edgeMeasures", "edgeID", "bad"));
    }

    @Path("/server/rest/stats/")
    public static class MockMeasuresServer {
        public static List<ImmutableMeasurement> measurements = Arrays.asList(
                new ImmutableMeasurement("edgeID", "Type1", 10.0, 5),
                new ImmutableMeasurement("edgeID", "Type2", 5.0, 10)
        );

        @GET
        @Path("/measures/edge/edgeID/2")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getMeasures() {
            return Response.ok(measurements).build();
        }

        @GET
        @Path("/measures/edge/invalidEdge/2")
        public Response getError() {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
