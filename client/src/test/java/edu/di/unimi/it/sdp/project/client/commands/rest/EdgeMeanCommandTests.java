package edu.di.unimi.it.sdp.project.client.commands.rest;

import com.sun.net.httpserver.HttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class EdgeMeanCommandTests extends RestCommandTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockMeanServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testMean() {
        EdgeMeanCommand command = new EdgeMeanCommand(target);
        assertEquals("Mean of the last 5 measurements for edge edgeID: 10.0",
                command.exec("edgeMean", "edgeID", "5"));
    }

    @Test
    public void testNoEdgeFound() {
        EdgeMeanCommand command = new EdgeMeanCommand(target);
        assertEquals("Invalid edge ID", command.exec("edgeMean", "invalidEdge", "5"));
    }

    @Test
    public void testNotEnoughParameters() {
        EdgeMeanCommand command = new EdgeMeanCommand(target);
        assertEquals("Not enough arguments: requires edge ID and number of measures",
                command.exec("edgeMean"));
    }

    @Test
    public void testInvalidNumber() {
        EdgeMeanCommand command = new EdgeMeanCommand(target);
        assertEquals("Number of measures must be a number",
                command.exec("edgeMean", "edgeID", "bad"));
    }

    @Path("/server/rest/stats/")
    public static class MockMeanServer {

        @GET
        @Path("/mean/edgeID/5")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getMean() {
            return Response.ok(10.0).build();
        }

        @GET
        @Path("/mean/invalidEdge/5")
        public Response getError() {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}