package edu.di.unimi.it.sdp.project.model.adapters;

import edu.di.unimi.it.sdp.project.model.GridPosition;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class GridPositionAdapter extends XmlAdapter<GridPositionAdapter.MutableGridPosition, GridPosition> {
    @Override
    public GridPosition unmarshal(MutableGridPosition mutableGridPosition) throws Exception {
        return new GridPosition(mutableGridPosition.getX(),
                mutableGridPosition.getY());
    }

    @Override
    public MutableGridPosition marshal(GridPosition gridPosition) throws Exception {
        MutableGridPosition pos = new MutableGridPosition();
        pos.setX(gridPosition.getX());
        pos.setY(gridPosition.getY());
        return pos;
    }

    public static class MutableGridPosition {
        private int x;
        private int y;

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }
    }
}
