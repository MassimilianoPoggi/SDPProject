package edu.di.unimi.it.sdp.project.model;

import edu.di.unimi.it.sdp.project.model.adapters.GridPositionAdapter;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Random;

@XmlJavaTypeAdapter(GridPositionAdapter.class)
public class GridPosition {
    public static final int GRID_EDGE_LENGTH = 100;
    public static final GridPosition UNKNOWN = new GridPosition();

    private final int x;
    private final int y;

    private GridPosition() {
        this.x = -1;
        this.y = -1;
    }

    public GridPosition(int x, int y) {
        if (x < 0 || y < 0 || x >= GRID_EDGE_LENGTH || y >= GRID_EDGE_LENGTH)
            throw new IllegalArgumentException("Invalid position");

        this.x = x;
        this.y = y;
    }

    public static GridPosition random() {
        Random r = new Random();
        return new GridPosition(r.nextInt(GRID_EDGE_LENGTH), r.nextInt(GRID_EDGE_LENGTH));
    }

    public int distance(GridPosition position) {
        return Math.abs(this.x - position.x) + Math.abs(this.y - position.y);
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

    @Override
    public String toString() {
        return x + "x" + y;
    }
}
