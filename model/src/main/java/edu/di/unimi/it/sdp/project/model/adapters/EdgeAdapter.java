package edu.di.unimi.it.sdp.project.model.adapters;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.net.InetAddress;

public class EdgeAdapter extends XmlAdapter<EdgeAdapter.MutableEdge, Edge> {
    @Override
    public Edge unmarshal(MutableEdge mutableEdge) throws Exception {
        return new Edge(mutableEdge.getId(),
                mutableEdge.getPosition(),
                mutableEdge.getIp(),
                mutableEdge.getSensorPort(),
                mutableEdge.getEdgePort());
    }

    @Override
    public MutableEdge marshal(Edge edge) throws Exception {
        MutableEdge e = new MutableEdge();
        e.setId(edge.getID());
        e.setPosition(edge.getPosition());
        e.setIp(edge.getIpAddress());
        e.setSensorPort(edge.getSensorPort());
        e.setEdgePort(edge.getEdgePort());
        return e;
    }

    public static class MutableEdge {
        private String id;
        private GridPosition position;
        private InetAddress ip;
        private int sensorPort;
        private int edgePort;

        public int getEdgePort() {
            return edgePort;
        }

        public void setEdgePort(int edgePort) {
            this.edgePort = edgePort;
        }

        public int getSensorPort() {
            return sensorPort;
        }

        public void setSensorPort(int sensorPort) {
            this.sensorPort = sensorPort;
        }

        public InetAddress getIp() {
            return ip;
        }

        public void setIp(InetAddress ip) {
            this.ip = ip;
        }

        public GridPosition getPosition() {
            return position;
        }

        public void setPosition(GridPosition position) {
            this.position = position;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}
