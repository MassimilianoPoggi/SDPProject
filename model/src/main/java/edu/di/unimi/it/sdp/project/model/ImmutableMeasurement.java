package edu.di.unimi.it.sdp.project.model;

import edu.di.unimi.it.sdp.project.model.adapters.MeasurementAdapter;
import edu.di.unimi.it.sdp.project.model.communications.Measurements.MeasurementMessage;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Objects;

@XmlJavaTypeAdapter(MeasurementAdapter.class)
public class ImmutableMeasurement implements Comparable<ImmutableMeasurement> {
    private String id;
    private String type;
    private double value;
    private long timestamp;

    public ImmutableMeasurement(MeasurementMessage message) {
        this.id = message.getId();
        this.type = message.getType();
        this.value = message.getValue();
        this.timestamp = message.getTimestamp();
    }

    public ImmutableMeasurement(String id, String type, double value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
        this.id = id;
        this.type = type;
    }

    public double getValue() {
        return value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    @Override
    public int compareTo(ImmutableMeasurement m) {
        Long thisTimestamp = timestamp;
        Long otherTimestamp = m.getTimestamp();
        return thisTimestamp.compareTo(otherTimestamp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImmutableMeasurement that = (ImmutableMeasurement) o;
        return Double.compare(that.value, value) == 0 &&
                timestamp == that.timestamp &&
                Objects.equals(id, that.id) &&
                Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, value, timestamp);
    }

    public MeasurementMessage toMessage() {
        return MeasurementMessage.newBuilder()
                .setId(id)
                .setType(type)
                .setValue(value)
                .setTimestamp(timestamp)
                .build();
    }

    @Override
    public String toString() {
        return "Measurement from " + id + " of type " + type + " at timestamp " + timestamp + ": " + value;
    }
}
