package edu.di.unimi.it.sdp.project.model;

import edu.di.unimi.it.sdp.project.model.adapters.EdgeAdapter;
import edu.di.unimi.it.sdp.project.model.communications.Edges;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

@XmlJavaTypeAdapter(EdgeAdapter.class)
public class Edge {
    private final String id;
    private final GridPosition position;
    private final InetAddress ipAddress;
    private final int sensorPort;
    private final int edgePort;

    public Edge(String id, GridPosition position, InetAddress edgeIP, int sensorPort, int edgePort) {
        this.id = id;
        this.position = position;
        this.ipAddress = edgeIP;
        this.sensorPort = sensorPort;
        this.edgePort = edgePort;
    }

    public Edge(Edges.EdgeMessage message) throws UnknownHostException {
        this.id = message.getId();
        if (message.hasX() && message.hasY())
            this.position = new GridPosition(message.getX(), message.getY());
        else
            this.position = GridPosition.UNKNOWN;
        this.ipAddress = InetAddress.getByName(message.getHost());
        this.sensorPort = message.getSensorPort();
        this.edgePort = message.getEdgePort();
    }

    public Edges.EdgeMessage toMessage() {
        return Edges.EdgeMessage.newBuilder()
                .setId(id)
                .setX(position.getX())
                .setY(position.getY())
                .setHost(ipAddress.getHostAddress())
                .setSensorPort(sensorPort)
                .setEdgePort(edgePort)
                .build();
    }

    public String getID() {
        return id;
    }

    public GridPosition getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Edge{" +
                "id='" + id + '\'' +
                ", position=" + position +
                ", ipAddress=" + ipAddress +
                ", sensorPort=" + sensorPort +
                ", edgePort=" + edgePort +
                '}';
    }

    public int getSensorPort() {
        return sensorPort;
    }

    public int getEdgePort() {
        return edgePort;
    }

    public InetAddress getIpAddress() {
        return ipAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return Objects.equals(id, edge.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Edge assignPosition(GridPosition position) {
        return new Edge(id, position, ipAddress, sensorPort, edgePort);
    }
}