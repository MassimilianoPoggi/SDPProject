package edu.di.unimi.it.sdp.project.model.adapters;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MeasurementAdapter extends XmlAdapter<MeasurementAdapter.MutableMeasurement, ImmutableMeasurement> {

    @Override
    public ImmutableMeasurement unmarshal(MutableMeasurement mutableMeasurement) throws Exception {
        return new ImmutableMeasurement(
                mutableMeasurement.getId(),
                mutableMeasurement.getType(),
                mutableMeasurement.getValue(),
                mutableMeasurement.getTimestamp()
        );
    }

    @Override
    public MutableMeasurement marshal(ImmutableMeasurement immutableMeasurement) throws Exception {
        MutableMeasurement m = new MutableMeasurement();
        m.setId(immutableMeasurement.getId());
        m.setType(immutableMeasurement.getType());
        m.setValue(immutableMeasurement.getValue());
        m.setTimestamp(immutableMeasurement.getTimestamp());
        return m;
    }

    public static class MutableMeasurement {
        private String id;
        private String type;
        private double value;
        private long timestamp;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(long timestamp) {
            this.timestamp = timestamp;
        }
    }
}
