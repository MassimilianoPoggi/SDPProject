package edu.di.unimi.it.sdp.project.model;

import org.junit.Test;

import java.net.InetAddress;

import static org.junit.Assert.assertEquals;

public class SerializationTests {
    @Test
    public void testMeasurementSerialization() {
        ImmutableMeasurement measurement = new ImmutableMeasurement("ID", "Type", 10.0, 10);
        assertEquals(measurement, new ImmutableMeasurement(measurement.toMessage()));
    }

    @Test
    public void testEdgeSerialization() throws Exception {
        Edge edge = new Edge("ID", new GridPosition(10, 10), InetAddress.getLocalHost(), 12345, 54321);
        assertEquals(edge, new Edge(edge.toMessage()));
    }
}
