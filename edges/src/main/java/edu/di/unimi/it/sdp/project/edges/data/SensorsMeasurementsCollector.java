package edu.di.unimi.it.sdp.project.edges.data;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import io.grpc.stub.StreamObserver;

import java.util.logging.Logger;

public class SensorsMeasurementsCollector extends MeasurementsCollectorGrpc.MeasurementsCollectorImplBase {
    private final SlidingWindowBuffer measurementsBuffer;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public SensorsMeasurementsCollector(SlidingWindowBuffer measurementsBuffer) {
        this.measurementsBuffer = measurementsBuffer;
    }

    @Override
    public StreamObserver<Measurements.MeasurementMessage> streamMeasurements(
            StreamObserver<Measurements.MeasurementMessage> responseObserver) {
        return new StreamObserver<Measurements.MeasurementMessage>() {
            @Override
            public void onNext(Measurements.MeasurementMessage value) {
                ImmutableMeasurement measurement = new ImmutableMeasurement(value);
                measurementsBuffer.addMeasurement(measurement);
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Sensor quit or found a new nearest edge");
            }

            @Override
            public void onCompleted() {
                logger.info("Sensor quit or found a new nearest edge");
            }
        };
    }
}