package edu.di.unimi.it.sdp.project.edges.election;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ElectionRunnable implements Runnable {
    private final Edge thisEdge;
    private final EdgesChannelsManager edgesChannelsManager;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private volatile boolean gotReply;

    public ElectionRunnable(Edge thisEdge, EdgesChannelsManager edgesChannelsManager) {
        this.thisEdge = thisEdge;
        this.edgesChannelsManager = edgesChannelsManager;
    }

    @Override
    public void run() {
        Collection<Thread> threads = new ArrayList<>();

        Collection<Edge> edgesToContact =
                edgesChannelsManager.getEdges().stream()
                    .filter((e) -> e.getID().compareTo(thisEdge.getID()) > 0)
                    .collect(Collectors.toSet());

        for (Edge e : edgesToContact) {
            logger.info("Sending election start message to " + e);
            Thread t = new Thread(new ElectionExchangeRunnable(e, this));
            threads.add(t);
            t.start();
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                logger.severe("Thread shouldn't be interrupted while waiting for election replies");
            }
        }

        if (!gotReply)
            informElectionCompleted();

        logger.info("Election protocol completed");
    }

    private void informElectionCompleted() {
        Collection<Thread> threads = new ArrayList<>();

        for (Edge e : edgesChannelsManager.getEdges()) {
            logger.info("Informing edge " + e + " I won the election");
            Thread t = new Thread(new ElectionCompletedRunnable(e));
            threads.add(t);
            t.start();
        }

        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                logger.severe("Thread shouldn't be interrupted while informing edges of election results");
            }
        }
    }

    private class ElectionExchangeRunnable implements Runnable {
        private final Edge edge;
        private final ElectionRunnable electionRunnable;

        private ElectionExchangeRunnable(Edge edge, ElectionRunnable electionRunnable) {
            this.edge = edge;
            this.electionRunnable = electionRunnable;
        }

        @Override
        public void run() {
            ManagedChannel channel = edgesChannelsManager.getChannel(edge);

            CoordinatorElectionGrpc.CoordinatorElectionBlockingStub stub =
                    CoordinatorElectionGrpc.newBlockingStub(channel);
            try {
                stub.startElection(thisEdge.toMessage());
                electionRunnable.gotReply = true;
            } catch (StatusRuntimeException e) {
                logger.info("Couldn't communicate with " + edge);
                edgesChannelsManager.removeEdge(edge);
            }
        }
    }

    private class ElectionCompletedRunnable implements Runnable {
        private final Edge edge;

        private ElectionCompletedRunnable(Edge edge) {
            this.edge = edge;
        }

        @Override
        public void run() {
            ManagedChannel channel = edgesChannelsManager.getChannel(edge);

            CoordinatorElectionGrpc.CoordinatorElectionBlockingStub stub =
                    CoordinatorElectionGrpc.newBlockingStub(channel);

            try {
                stub.nodeElected(thisEdge.toMessage());
            } catch (StatusRuntimeException e) {
                logger.info("Couldn't communicate with " + edge);
                edgesChannelsManager.removeEdge(edge);
            }
        }
    }
}