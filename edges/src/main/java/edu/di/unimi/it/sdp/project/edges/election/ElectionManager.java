package edu.di.unimi.it.sdp.project.edges.election;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.coordinator.CoordinatorServicesManager;
import edu.di.unimi.it.sdp.project.model.Edge;

public class ElectionManager {
    private final Edge thisEdge;
    private final EdgesChannelsManager edgesChannelsManager;
    private final CoordinatorServicesManager coordinatorServicesManager;

    private State currentState;
    private Edge currentCoordinator;

    public ElectionManager(Edge thisEdge, EdgesChannelsManager edgesChannelsManager,
                           CoordinatorServicesManager coordinatorServicesManager) {
        this.thisEdge = thisEdge;
        this.edgesChannelsManager = edgesChannelsManager;
        this.coordinatorServicesManager = coordinatorServicesManager;
        this.currentState = State.JOINING;
        this.currentCoordinator = null;
    }

    public synchronized void startElection() {
        if (this.currentState == State.ELECTING)
            return;

        this.currentState = State.ELECTING;

        new Thread(new ElectionRunnable(thisEdge, edgesChannelsManager)).start();
    }

    public synchronized Edge getCoordinator() throws InterruptedException {
        if (this.currentState == State.ELECTED && this.currentCoordinator == null)
            startElection();

        while (this.currentState != State.ELECTED)
            wait();

        return currentCoordinator;
    }

    public synchronized void setCoordinator(Edge e) {
        if (e.equals(thisEdge))
            coordinatorServicesManager.startCoordinatorServices();

        currentCoordinator = e;
        currentState = State.ELECTED;
        notifyAll();
    }

    public synchronized void finishedJoining() {
        currentState = State.ELECTED;
        if (currentCoordinator == null)
            setCoordinator(thisEdge);
    }

    public synchronized boolean isInElection() {
        return this.currentState == State.ELECTING;
    }

    private enum State {
        ELECTING,
        ELECTED,
        JOINING
    }
}
