package edu.di.unimi.it.sdp.project.edges.grid;

import edu.di.unimi.it.sdp.project.model.Edge;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

public class EdgeRemovalManager {
    private final Edge thisEdge;
    private final WebTarget target;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public EdgeRemovalManager(Edge thisEdge, WebTarget target) {
        this.thisEdge = thisEdge;
        this.target = target;
    }

    public void removeFromGrid() {
        logger.info("Sending removal request for edge " + thisEdge);
        try {
            Response response =
                    target.path("edges").path(thisEdge.getID())
                            .request()
                            .delete();

            switch (response.getStatusInfo().toEnum()) {
                case OK:
                    logger.info("Edge " + thisEdge + " successfully removed from the grid");
                    break;
                case NOT_FOUND:
                    logger.severe("Tried to remove edge " + thisEdge + " from the grid, but it was already removed");
                    break;
                default:
                    logger.severe("Unexpected response " + response + " from server while removing edge " + thisEdge);
                    break;
            }
        } catch (Exception e) {
            logger.warning("Server isn't responding: is the address correct?");
        }
    }
}
