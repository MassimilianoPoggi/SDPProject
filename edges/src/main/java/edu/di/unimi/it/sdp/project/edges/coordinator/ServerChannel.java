package edu.di.unimi.it.sdp.project.edges.coordinator;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

public class ServerChannel extends Thread {
    private final WebTarget target;
    private final MeasurementsStore measurements;
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private volatile boolean shutdown;
    private ImmutableMeasurement lastGlobalMeasurement;

    public ServerChannel(WebTarget target, MeasurementsStore measurements) {
        this.target = target;
        this.measurements = measurements;
        this.lastGlobalMeasurement = null;
    }

    @Override
    public void run() {
        while (!shutdown) {
            try {
                List<ImmutableMeasurement> newMeasurements = measurements.getNewMeasurements();

                if (!newMeasurements.isEmpty()) {
                    ImmutableMeasurement global =
                            new ImmutableMeasurement(
                                    "Coordinator",
                                    newMeasurements.get(0).getType(),
                                    newMeasurements.stream().mapToDouble(ImmutableMeasurement::getValue).average().getAsDouble(),
                                    millisecondsOfDay());

                    synchronized (this) {
                        lastGlobalMeasurement = global;
                    }

                    sendGlobalMeasurement(global);
                    sendLocalMeasurements(newMeasurements);
                } else {
                    logger.info("No measurements were added since last global calculation");
                }

                Thread.sleep(5000);
            } catch (InterruptedException e) {
                logger.info("Interrupted to shut down");
            }
        }
    }

    public synchronized ImmutableMeasurement getLastGlobalMeasurement() {
        return lastGlobalMeasurement;
    }

    public void shutdown() {
        this.shutdown = true;
    }

    private void sendLocalMeasurements(List<ImmutableMeasurement> newMeasurements) {
        for (ImmutableMeasurement m : newMeasurements) {
            logger.info("Sending measurement " + m + " for edge " + m.getId());
            try {
                Response response = target.path("stats").path("measures")
                        .path("edge").path(m.getId())
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(Entity.entity(m, MediaType.APPLICATION_JSON_TYPE));

                if (response.getStatusInfo().toEnum() != Response.Status.OK)
                    logger.severe("Unexpected response " + response + " while sending measurement " +
                            m + " for edge " + m.getId());
            } catch (Exception e) {
                logger.warning("Server isn't responding: is the address correct?");
            }
        }
    }

    private void sendGlobalMeasurement(ImmutableMeasurement global) {
        logger.info("Sending global measurement " + global);
        try {
            Response response = target.path("stats").path("measures")
                    .path("global").request(MediaType.APPLICATION_JSON_TYPE)
                    .put(Entity.entity(global, MediaType.APPLICATION_JSON_TYPE));

            if (response.getStatusInfo().toEnum() != Response.Status.OK)
                logger.severe("Unexpected response " + response + " while sending global measurement " + global);
        } catch (Exception e) {
            logger.warning("Server isn't responding: is the address correct?");
        }
    }

    private long millisecondsOfDay() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return (System.currentTimeMillis() - c.getTimeInMillis());
    }
}
