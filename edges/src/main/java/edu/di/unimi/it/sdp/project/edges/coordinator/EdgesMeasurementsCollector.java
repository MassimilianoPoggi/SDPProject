package edu.di.unimi.it.sdp.project.edges.coordinator;

import edu.di.unimi.it.sdp.project.edges.EdgePanel;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import io.grpc.stub.StreamObserver;

import java.util.logging.Logger;

public class EdgesMeasurementsCollector extends MeasurementsCollectorGrpc.MeasurementsCollectorImplBase {
    private final MeasurementsStore measurementStore;
    private final ServerChannel serverChannel;
    private final EdgePanel panel;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public EdgesMeasurementsCollector(MeasurementsStore measurementStore, ServerChannel serverChannel, EdgePanel panel) {
        this.measurementStore = measurementStore;
        this.serverChannel = serverChannel;
        this.panel = panel;
    }

    @Override
    public StreamObserver<Measurements.MeasurementMessage> streamMeasurements(
            StreamObserver<Measurements.MeasurementMessage> responseObserver) {
        return new StreamObserver<Measurements.MeasurementMessage>() {
            @Override
            public void onNext(Measurements.MeasurementMessage value) {
                ImmutableMeasurement measurement = new ImmutableMeasurement(value);
                logger.info("Received local measurement " + measurement + " from edge " + measurement.getId());
                measurementStore.addMeasurement(measurement);
                panel.receivedLocalMeasurement(measurement);

                ImmutableMeasurement lastGlobalMeasurement = serverChannel.getLastGlobalMeasurement();
                if (lastGlobalMeasurement != null) {
                    logger.info("Sending back global measurement " + lastGlobalMeasurement);
                    responseObserver.onNext(lastGlobalMeasurement.toMessage());
                }
            }

            @Override
            public void onError(Throwable t) {
                logger.info("Edge quit the network");
            }

            @Override
            public void onCompleted() {
                logger.info("Edge quit the network");
            }
        };
    }
}
