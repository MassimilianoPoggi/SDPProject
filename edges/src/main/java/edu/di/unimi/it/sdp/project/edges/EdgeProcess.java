package edu.di.unimi.it.sdp.project.edges;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.coordinator.CoordinatorServicesManager;
import edu.di.unimi.it.sdp.project.edges.data.CoordinatorChannel;
import edu.di.unimi.it.sdp.project.edges.data.SensorsMeasurementsCollector;
import edu.di.unimi.it.sdp.project.edges.data.SlidingWindowBuffer;
import edu.di.unimi.it.sdp.project.edges.election.CoordinatorElectionService;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.edges.greet.GreetEdgeService;
import edu.di.unimi.it.sdp.project.edges.greet.GreetManager;
import edu.di.unimi.it.sdp.project.edges.grid.EdgeInsertionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.util.MutableHandlerRegistry;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Scanner;

public class EdgeProcess {
    public static void main(String... args) {
        if (args.length < 4) {
            System.out.println("Not enough arguments. Needed: ID, sensor port, edge port and server address");
            return;
        }

        Edge thisEdge = parseEdgeAttributes(args);
        ShutdownHelper shutdownHelper = new ShutdownHelper();

        Client client = getRestClient(shutdownHelper);
        WebTarget target = client.target(UriBuilder.fromUri("http://" + args[3] + "/server/rest").build());

        SlidingWindowBuffer measurementsBuffer = new SlidingWindowBuffer(thisEdge);
        if (!startSensorsChannel(thisEdge, measurementsBuffer, shutdownHelper)) {
            System.out.println("Unable to start server to communicate with sensors");
            shutdownHelper.shutdown();
            return;
        }

        EdgesChannelsManager edgesChannelsManager = getEdgesChannelsManager(shutdownHelper);
        MutableHandlerRegistry mutableHandlerRegistry = new MutableHandlerRegistry();
        EdgePanel edgePanel = new EdgePanel();

        CoordinatorServicesManager coordinatorServicesManager =
                getCoordinatorServicesManager(target, mutableHandlerRegistry, edgePanel, shutdownHelper);

        ElectionManager electionManager = new ElectionManager(thisEdge, edgesChannelsManager, coordinatorServicesManager);

        if (!startEdgesChannel(thisEdge, edgesChannelsManager, electionManager, mutableHandlerRegistry, shutdownHelper)) {
            System.out.println("Unable to start server to communicate with edges");
            shutdownHelper.shutdown();
            return;
        }

        startCoordinatorChannel(edgesChannelsManager, measurementsBuffer, electionManager, edgePanel, shutdownHelper);

        List<Edge> otherEdges = insertEdgeIntoGrid(thisEdge, target, edgesChannelsManager, shutdownHelper);
        if (otherEdges == null) {
            System.out.println("Unable to insert edge into the grid");
            shutdownHelper.shutdown();
            return;
        }

        greetEdges(thisEdge, otherEdges, edgesChannelsManager, electionManager);

        Scanner sc = new Scanner(new InputStreamReader(System.in));

        while (!sc.nextLine().equals("exit"))
            System.out.println("Type exit to exit the application");

        waitElectionEnd(electionManager);

        shutdownHelper.shutdown();
    }

    private static EdgesChannelsManager getEdgesChannelsManager(ShutdownHelper shutdownHelper) {
        EdgesChannelsManager edgesChannelsManager = new EdgesChannelsManager();
        shutdownHelper.startedCloseable(edgesChannelsManager);
        return edgesChannelsManager;
    }

    private static void waitElectionEnd(ElectionManager electionManager) {
        synchronized (electionManager) {
            while (electionManager.isInElection()) {
                try {
                    electionManager.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void startCoordinatorChannel(EdgesChannelsManager edgesChannelsManager,
                                                   SlidingWindowBuffer measurementsBuffer,
                                                   ElectionManager electionManager,
                                                   EdgePanel edgePanel,
                                                   ShutdownHelper shutdownHelper) {
        CoordinatorChannel channel =
                new CoordinatorChannel(
                        edgesChannelsManager,
                        measurementsBuffer,
                        electionManager,
                        edgePanel);

        channel.start();

        shutdownHelper.startedCloseable(channel);
    }

    private static CoordinatorServicesManager getCoordinatorServicesManager(WebTarget target,
                                                                            MutableHandlerRegistry mutableHandlerRegistry,
                                                                            EdgePanel edgePanel,
                                                                            ShutdownHelper shutdownHelper) {
        CoordinatorServicesManager manager = new CoordinatorServicesManager(target, mutableHandlerRegistry, edgePanel);
        shutdownHelper.startedCloseable(manager);
        return manager;
    }

    private static void greetEdges(Edge thisEdge, List<Edge> otherEdges, EdgesChannelsManager edgesChannelsManager,
                                   ElectionManager electionManager) {
        GreetManager greetManager = new GreetManager(thisEdge, otherEdges, edgesChannelsManager, electionManager);
        greetManager.greet();
    }

    private static boolean startEdgesChannel(Edge thisEdge, EdgesChannelsManager edgesChannelsManager,
                                             ElectionManager electionManager,
                                             MutableHandlerRegistry mutableHandlerRegistry,
                                             ShutdownHelper shutdownHelper) {
        Server server =
                ServerBuilder
                        .forPort(thisEdge.getEdgePort())
                        .addService(new GreetEdgeService(edgesChannelsManager, electionManager))
                        .addService(new CoordinatorElectionService(electionManager))
                        .fallbackHandlerRegistry(mutableHandlerRegistry)
                        .build();

        try {
            server.start();
        } catch (IOException e) {
            return false;
        }

        shutdownHelper.startedServer(server);
        return true;
    }

    private static boolean startSensorsChannel(Edge thisEdge, SlidingWindowBuffer measurementsBuffer,
                                            ShutdownHelper shutdownHelper) {
        Server server =
                ServerBuilder
                        .forPort(thisEdge.getSensorPort())
                        .addService(new SensorsMeasurementsCollector(measurementsBuffer))
                        .build();

        try {
            server.start();
        } catch (IOException e) {
            return false;
        }

        shutdownHelper.startedServer(server);
        return true;
    }

    private static Client getRestClient(ShutdownHelper shutdownHelper) {
        Client client = ClientBuilder.newClient();
        shutdownHelper.startedClient(client);
        return client;
    }

    private static List<Edge> insertEdgeIntoGrid(Edge thisEdge, WebTarget target,
                                                 EdgesChannelsManager edgesChannelsManager,
                                                 ShutdownHelper shutdownHelper) {
        EdgeInsertionManager edgeInsertionManager = new EdgeInsertionManager(thisEdge, target,
                edgesChannelsManager);

        List<Edge> otherEdges = edgeInsertionManager.insertIntoGrid();
        if (otherEdges == null) {
            return null;
        }

        shutdownHelper.insertedEdge(thisEdge, target);
        return otherEdges;
    }

    private static Edge parseEdgeAttributes(String... args) {
        int sensorPort = parsePortNumber(args[1]);
        int edgePort = parsePortNumber(args[2]);

        InetAddress localhost = getLocalHostAddress();

        return new Edge(args[0], GridPosition.UNKNOWN, localhost, sensorPort, edgePort);
    }

    private static int parsePortNumber(String arg) {
        int port = 0;
        try {
            port = Integer.parseInt(arg);
            if (port < 1 || port > 0xFFFF) {
                System.out.println("Port number out of range");
                System.exit(0);
            }
        } catch (NumberFormatException e) {
            System.out.println("Port number must be a number");
            System.exit(0);
        }
        return port;
    }

    private static InetAddress getLocalHostAddress() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            System.out.println("Couldn't get localhost address");
            System.exit(0);
        }
        return null; //really?
    }
}
