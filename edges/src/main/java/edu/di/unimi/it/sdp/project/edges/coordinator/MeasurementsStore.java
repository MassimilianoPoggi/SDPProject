package edu.di.unimi.it.sdp.project.edges.coordinator;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;

import java.util.ArrayList;
import java.util.List;

public class MeasurementsStore {
    private final List<ImmutableMeasurement> measurements;

    public MeasurementsStore() {
        measurements = new ArrayList<>();
    }

    public synchronized void addMeasurement(ImmutableMeasurement measurement) {
        measurements.add(measurement);
    }

    public synchronized List<ImmutableMeasurement> getNewMeasurements() {
        List<ImmutableMeasurement> result = new ArrayList<>(measurements);
        measurements.clear();
        return result;
    }
}
