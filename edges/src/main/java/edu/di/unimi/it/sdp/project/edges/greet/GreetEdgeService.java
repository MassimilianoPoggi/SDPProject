package edu.di.unimi.it.sdp.project.edges.greet;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.communications.Edges;
import io.grpc.stub.StreamObserver;

import java.net.UnknownHostException;
import java.util.logging.Logger;

public class GreetEdgeService extends GreetEdgeGrpc.GreetEdgeImplBase {
    private final EdgesChannelsManager edgesChannelsManager;
    private final ElectionManager electionManager;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public GreetEdgeService(EdgesChannelsManager edgesChannelsManager, ElectionManager electionManager) {
        this.edgesChannelsManager = edgesChannelsManager;
        this.electionManager = electionManager;
    }

    @Override
    public void greet(Edges.EdgeMessage request, StreamObserver<Edges.EdgeMessage> responseObserver) {
        try {
            Edge edge = new Edge(request);
            edgesChannelsManager.addEdge(edge);
            Edge coordinator = electionManager.getCoordinator();
            logger.info("Received hello from " + edge + ": replying with current coordinator "+ coordinator);
            responseObserver.onNext(coordinator.toMessage());
            responseObserver.onCompleted();
        } catch (UnknownHostException e) {
            logger.severe("Couldn't resolve hostname for greeted edge");
        } catch (InterruptedException e) {
            logger.severe("Thread shouldn't get interrupted while asking for coordinator");
        }
    }
}
