package edu.di.unimi.it.sdp.project.edges.greet;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.communications.Edges;
import io.grpc.ManagedChannel;
import io.grpc.StatusRuntimeException;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

public class GreetManager {
    private final Edge thisEdge;
    private final List<Edge> otherEdges;
    private final EdgesChannelsManager edgesChannelsManager;
    private final ElectionManager electionManager;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public GreetManager(Edge thisEdge, List<Edge> otherEdges,
                        EdgesChannelsManager edgesChannelsManager, ElectionManager electionManager) {
        this.thisEdge = thisEdge;
        this.otherEdges = otherEdges;
        this.edgesChannelsManager = edgesChannelsManager;
        this.electionManager = electionManager;
    }

    public void greet() {
        Collection<Thread> greetThreads = new ArrayList<>();
        for (Edge edge : otherEdges) {
            Thread t = new Thread(new GreetRunnable(edge));
            greetThreads.add(t);
            t.start();
        }

        for (Thread t : greetThreads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                logger.severe("Thread shouldn't ever get interrupted while waiting for greet replies");
            }
        }

        electionManager.finishedJoining();
        logger.info("Greet protocol completed");
    }

    private class GreetRunnable implements Runnable {
        private final Edge edge;

        public GreetRunnable(Edge edge) {
            this.edge = edge;
        }

        @Override
        public void run() {
            if (edge.equals(thisEdge))
                return;

            ManagedChannel channel = edgesChannelsManager.getChannel(edge);

            GreetEdgeGrpc.GreetEdgeBlockingStub stub = GreetEdgeGrpc.newBlockingStub(channel);
            try {
                logger.info("Sending greet message to " + edge);
                Edges.EdgeMessage reply = stub.greet(thisEdge.toMessage());
                Edge coordinator = new Edge(reply);
                logger.info("Got greet reply from edge " + edge + ": sent " + coordinator + " as coordinator");
                electionManager.setCoordinator(coordinator);
            } catch (StatusRuntimeException e) {
                logger.info("Couldn't communicate with " + edge);
                edgesChannelsManager.removeEdge(edge);
            } catch (UnknownHostException e) {
                logger.severe("Couldn't resolve edge address");
            }
        }
    }
}