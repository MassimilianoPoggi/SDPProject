package edu.di.unimi.it.sdp.project.edges.data;

import edu.di.unimi.it.sdp.project.edges.EdgePanel;
import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;

import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Logger;

public class CoordinatorChannel extends Thread implements Closeable {
    private final EdgesChannelsManager channelsManager;
    private final SlidingWindowBuffer buffer;
    private final ElectionManager electionManager;
    private final EdgePanel panel;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private volatile boolean shutdown;
    private Edge currentCoordinator;
    private StreamObserver<Measurements.MeasurementMessage> streamObserver;

    public CoordinatorChannel(EdgesChannelsManager channelsManager, SlidingWindowBuffer buffer,
                              ElectionManager electionManager, EdgePanel panel) {
        this.channelsManager = channelsManager;
        this.buffer = buffer;
        this.electionManager = electionManager;
        this.panel = panel;
        this.currentCoordinator = null;
        this.streamObserver = null;
    }

    @Override
    public void run() {
        while (!shutdown) {
            try {
                ImmutableMeasurement measurement = buffer.getAggregatedMeasurement();
                synchronized (electionManager) {
                    updateStreamObserver(electionManager.getCoordinator());
                }

                panel.receivedLocalMeasurement(measurement);

                if (streamObserver != null) {
                    logger.info("Sending local measurement " + measurement);
                    streamObserver.onNext(measurement.toMessage());
                }

            } catch (InterruptedException e) {
                logger.info("Interrupted to close");
            }
        }
    }

    private void updateStreamObserver(Edge newCoordinator) {
        if (newCoordinator != null && !newCoordinator.equals(currentCoordinator)) {
            ManagedChannel channel = channelsManager.getChannel(newCoordinator);

            if (channel == null) {
                streamObserver = null;
                currentCoordinator = null;
                logger.severe("No channel found for edge " + newCoordinator);
                return;
            }

            currentCoordinator = newCoordinator;
            MeasurementsCollectorGrpc.MeasurementsCollectorStub stub = MeasurementsCollectorGrpc.newStub(channel);

            streamObserver = stub.streamMeasurements(new StreamObserver<Measurements.MeasurementMessage>() {
                @Override
                public void onNext(Measurements.MeasurementMessage value) {
                    ImmutableMeasurement measurement = new ImmutableMeasurement(value);
                    logger.info("Received global measurement " + measurement);
                    panel.receivedGlobalMeasurement(measurement);
                }

                @Override
                public void onError(Throwable t) {
                    logger.info("Coordinator quit the network");
                    onCoordinatorQuit();
                }

                @Override
                public void onCompleted() {
                    logger.info("Coordinator quit the network");
                    onCoordinatorQuit();
                }

                private void onCoordinatorQuit() {
                    synchronized (electionManager) {
                        channelsManager.removeEdge(currentCoordinator);
                        electionManager.startElection();
                        currentCoordinator = null;
                        streamObserver = null;
                    }
                }
            });
        }
    }

    @Override
    public void close() throws IOException {
        this.shutdown = true;
        this.interrupt();
    }
}
