package edu.di.unimi.it.sdp.project.edges.coordinator;

import edu.di.unimi.it.sdp.project.edges.EdgePanel;
import io.grpc.util.MutableHandlerRegistry;

import javax.ws.rs.client.WebTarget;
import java.io.Closeable;
import java.io.IOException;

public class CoordinatorServicesManager implements Closeable {
    private final WebTarget target;
    private final MutableHandlerRegistry servicesRegistry;
    private final EdgePanel panel;
    private ServerChannel serverChannel;

    public CoordinatorServicesManager(WebTarget target, MutableHandlerRegistry servicesRegistry, EdgePanel panel) {
        this.target = target;
        this.servicesRegistry = servicesRegistry;
        this.panel = panel;
        this.serverChannel = null;
    }

    public void startCoordinatorServices() {
        MeasurementsStore measurementsStore = new MeasurementsStore();
        serverChannel = new ServerChannel(target, measurementsStore);
        EdgesMeasurementsCollector edgesMeasurementsCollector =
                new EdgesMeasurementsCollector(
                        measurementsStore,
                        serverChannel,
                        panel);

        servicesRegistry.addService(edgesMeasurementsCollector);
        serverChannel.start();
    }

    @Override
    public void close() throws IOException {
        if (serverChannel != null) {
            serverChannel.shutdown();
            serverChannel.interrupt();
        }
    }
}
