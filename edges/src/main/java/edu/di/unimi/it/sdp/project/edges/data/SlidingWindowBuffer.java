package edu.di.unimi.it.sdp.project.edges.data;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Queue;

public class SlidingWindowBuffer {
    public final static int SLIDING_WINDOW_SIZE = 40;
    public final static int SLIDING_WINDOW_OVERLAP = 20;

    private final ImmutableMeasurement[] measurements;
    private final Queue<ImmutableMeasurement> aggregatedMeasurements;
    private final Edge thisEdge;
    private int index;
    private int validElements;

    public SlidingWindowBuffer(Edge thisEdge) {
        this.thisEdge = thisEdge;
        this.aggregatedMeasurements = new ArrayDeque<>();
        this.validElements = 0;
        this.index = 0;
        this.measurements = new ImmutableMeasurement[SLIDING_WINDOW_SIZE];
    }

    public void addMeasurement(ImmutableMeasurement m) {
        synchronized (measurements) {
            measurements[index++] = m;
            index %= SLIDING_WINDOW_SIZE;

            if (++validElements == SLIDING_WINDOW_SIZE) {
                addAggregatedMeasurement();
                validElements -= (SLIDING_WINDOW_SIZE - SLIDING_WINDOW_OVERLAP);
            }
        }
    }

    private void addAggregatedMeasurement() {
        ImmutableMeasurement aggregatedMeasurement =
                new ImmutableMeasurement(
                        thisEdge.getID(),
                        measurements[0].getType(),
                        Arrays.stream(measurements).mapToDouble(ImmutableMeasurement::getValue).average().getAsDouble(),
                        millisecondsOfDay());

        synchronized (aggregatedMeasurements) {
            aggregatedMeasurements.add(aggregatedMeasurement);
            aggregatedMeasurements.notifyAll();
        }
    }

    private long millisecondsOfDay() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return (System.currentTimeMillis() - c.getTimeInMillis());
    }

    public ImmutableMeasurement getAggregatedMeasurement() throws InterruptedException {
        synchronized (aggregatedMeasurements) {
            while (aggregatedMeasurements.isEmpty())
                aggregatedMeasurements.wait();

            return aggregatedMeasurements.remove();
        }
    }
}
