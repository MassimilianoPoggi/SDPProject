package edu.di.unimi.it.sdp.project.edges.grid;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.server.edges.EdgeManager;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

public class EdgeInsertionManager {
    private static final int NUMBER_OF_RETRIES = 10;

    private final WebTarget target;
    private final EdgesChannelsManager edgesChannelsManager;
    private final Edge thisEdge;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public EdgeInsertionManager(Edge thisEdge, WebTarget target, EdgesChannelsManager edgesChannelsManager) {
        this.thisEdge = thisEdge;
        this.target = target;
        this.edgesChannelsManager = edgesChannelsManager;
    }

    public List<Edge> insertIntoGrid() {
        for (int i = 0; i < NUMBER_OF_RETRIES; i++) {
            Edge positionedEdge = thisEdge.assignPosition(GridPosition.random());

            logger.info("Trying to insert edge "+ positionedEdge + " into the grid");
            try {
                Response response =
                        target.path("edges")
                                .request(MediaType.APPLICATION_JSON_TYPE)
                                .put(Entity.json(positionedEdge));

                switch (response.getStatusInfo().toEnum()) {
                    case CREATED:
                        logger.info("Edge " + positionedEdge + " was successfully inserted into the grid");
                        List<Edge> otherEdges = response.readEntity(new GenericType<List<Edge>>() {});
                        for (Edge edge: otherEdges)
                            edgesChannelsManager.addEdge(edge);
                        return otherEdges;
                    case CONFLICT:
                        if (!canResolveConflict(response)) {
                            logger.info("Edge " + positionedEdge + " has an ID conflict with another edge in the grid");
                            return null;
                        }
                        logger.info("Edge " + positionedEdge + " has a position conflict with another edge in the grid");
                        break;
                    default:
                        logger.severe("Unexpected response " + response + " from server while inserting edge " +
                                positionedEdge + " into the grid");
                        return null;
                }
            } catch (Exception e) {
                logger.warning("Server isn't responding: is the address correct?");
                return null;
            }
        }

        return null;
    }

    private boolean canResolveConflict(Response response) {
        EdgeManager.Conflict reason = response.readEntity(EdgeManager.Conflict.class);
        return reason == EdgeManager.Conflict.TOO_CLOSE;
    }
}
