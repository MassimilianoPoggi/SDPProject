package edu.di.unimi.it.sdp.project.edges;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;

public class EdgePanel {
    public synchronized void receivedGlobalMeasurement(ImmutableMeasurement measurement) {
        System.out.println("New global measurement: " + measurement);
    }

    public synchronized void receivedLocalMeasurement(ImmutableMeasurement measurement) {
        System.out.println("New local measurement: " + measurement);
    }
}
