package edu.di.unimi.it.sdp.project.edges;

import edu.di.unimi.it.sdp.project.edges.grid.EdgeRemovalManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import io.grpc.Server;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Logger;

public class ShutdownHelper {
    private final Collection<Client> clients;
    private final Collection<Server> servers;
    private final Collection<Closeable> closeables;
    private Edge thisEdge;
    private WebTarget server;

    public ShutdownHelper() {
        clients = new ArrayList<>();
        servers = new ArrayList<>();
        closeables = new ArrayList<>();
    }

    public void shutdown() {
        if (thisEdge != null)
            removeEdgeFromGrid();

        for (Closeable closeable : closeables) {
            try {
                closeable.close();
            } catch (IOException e) {
                Logger.getLogger(this.getClass().getName())
                        .severe("Exception while closing resource " + e);
            }
        }

        for (Server server : servers) {
            server.shutdownNow();
            try {
                server.awaitTermination();
            } catch (InterruptedException e) {
                Logger.getLogger(this.getClass().getName())
                        .severe("Thread shouldn't be getting interrupted while waiting for GRPC server shutdown");
            }
        }

        for (Client client : clients) {
            client.close();
        }
    }

    public void startedClient(Client client) {
        clients.add(client);
    }

    public void insertedEdge(Edge thisEdge, WebTarget server) {
        this.thisEdge = thisEdge;
        this.server = server;
    }

    public void startedServer(Server server) {
        servers.add(server);
    }

    private void removeEdgeFromGrid() {
        EdgeRemovalManager edgeRemovalManager = new EdgeRemovalManager(thisEdge, server);
        edgeRemovalManager.removeFromGrid();
    }

    public void startedCloseable(Closeable closeable) {
        closeables.add(closeable);
    }
}