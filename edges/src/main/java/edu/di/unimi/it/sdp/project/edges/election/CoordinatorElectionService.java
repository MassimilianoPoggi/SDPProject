package edu.di.unimi.it.sdp.project.edges.election;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.communications.Edges;
import io.grpc.stub.StreamObserver;

import java.net.UnknownHostException;
import java.util.logging.Logger;

public class CoordinatorElectionService extends CoordinatorElectionGrpc.CoordinatorElectionImplBase {

    private final ElectionManager electionManager;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    public CoordinatorElectionService(ElectionManager electionManager) {
        this.electionManager = electionManager;
    }

    @Override
    public void startElection(Edges.EdgeMessage request, StreamObserver<Edges.EdgeMessage> responseObserver) {
        logger.info("Received election start message");
        electionManager.startElection();
        responseObserver.onNext(request);
        responseObserver.onCompleted();
    }

    @Override
    public void nodeElected(Edges.EdgeMessage request, StreamObserver<Edges.EdgeMessage> responseObserver) {
        try {
            Edge coordinator = new Edge(request);
            logger.info("Received election end message. Edge " + coordinator + " was elected");
            electionManager.setCoordinator(coordinator);
        } catch (UnknownHostException e) {
            logger.severe("Couldn't resolve address for elected coordinator");
        }
        responseObserver.onNext(request);
        responseObserver.onCompleted();
    }
}
