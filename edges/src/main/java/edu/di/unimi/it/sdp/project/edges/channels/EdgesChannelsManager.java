package edu.di.unimi.it.sdp.project.edges.channels;

import edu.di.unimi.it.sdp.project.model.Edge;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

public class EdgesChannelsManager implements Closeable {
    private final HashMap<String, Edge> edges;
    private final HashMap<String, ManagedChannel> channels;

    public EdgesChannelsManager() {
        edges = new HashMap<>();
        channels = new HashMap<>();
    }

    public synchronized void addEdge(Edge e) {
        edges.put(e.getID(), e);
        channels.put(
                e.getID(),
                ManagedChannelBuilder
                        .forAddress(e.getIpAddress().getHostAddress(), e.getEdgePort())
                        .usePlaintext()
                        .build());
    }

    public synchronized void removeEdge(Edge e) {
        if (channels.containsKey(e.getID())) {
            ManagedChannel channel = channels.get(e.getID());
            channel.shutdownNow();
            edges.remove(e.getID());
            channels.remove(e.getID());
        }
    }

    public synchronized ManagedChannel getChannel(Edge e) {
        if (!channels.containsKey(e.getID()))
            throw new IllegalArgumentException("No channel found");

        return channels.get(e.getID());
    }

    public synchronized Collection<Edge> getEdges() {
        return Collections.unmodifiableCollection(edges.values());
    }

    @Override
    public synchronized void close() throws IOException {
        for (ManagedChannel channel : channels.values())
            channel.shutdownNow();

        channels.clear();
        edges.clear();
    }
}
