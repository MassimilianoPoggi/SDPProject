package edu.di.unimi.it.sdp.project.edges.coordinator;

import edu.di.unimi.it.sdp.project.edges.EdgePanel;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class EdgesMeasurementsCollectorTests {
    private static final int SERVER_PORT = 8080;
    private static Server server;
    private static MeasurementsStore measurementStore;
    private static ServerChannel serverChannel;
    private static EdgePanel edgePanel;


    @BeforeClass
    public static void setUpServer() throws Exception {
        measurementStore = mock(MeasurementsStore.class);
        serverChannel = mock(ServerChannel.class);
        edgePanel = mock(EdgePanel.class);

        server = ServerBuilder
                .forPort(SERVER_PORT)
                .addService(new EdgesMeasurementsCollector(measurementStore, serverChannel, edgePanel))
                .build();
        server.start();
    }

    @AfterClass
    public static void tearDownServer() {
        server.shutdown();
    }

    @Before
    public void resetMocks() {
        reset(measurementStore, serverChannel, edgePanel);
    }

    @Test
    public void testSuccessfulReceive() throws Exception {
        ImmutableMeasurement measurement = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement globalMeasurement = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        when(serverChannel.getLastGlobalMeasurement()).thenReturn(globalMeasurement);

        ManagedChannel channel =
                ManagedChannelBuilder
                        .forAddress("localhost", SERVER_PORT)
                        .usePlaintext()
                        .build();

        MeasurementsCollectorGrpc.MeasurementsCollectorStub stub = MeasurementsCollectorGrpc.newStub(channel);
        MockStreamObserver mockStreamObserver = new MockStreamObserver(measurement);

        StreamObserver<Measurements.MeasurementMessage> streamObserver = stub.streamMeasurements(mockStreamObserver);

        streamObserver.onNext(measurement.toMessage());

        synchronized (measurement) {
            if (!mockStreamObserver.called)
                measurement.wait();
        }

        assertEquals(globalMeasurement, mockStreamObserver.receivedMeasurement);
        verify(measurementStore).addMeasurement(measurement);
        verifyNoMoreInteractions(measurementStore);
        verify(edgePanel).receivedLocalMeasurement(measurement);
        verifyNoMoreInteractions(edgePanel);
        channel.shutdownNow();
    }

    private class MockStreamObserver implements StreamObserver<Measurements.MeasurementMessage> {
        private final Object lock;
        private boolean called = false;
        private ImmutableMeasurement receivedMeasurement;

        private MockStreamObserver(Object lock) {
            this.lock = lock;
        }

        @Override
        public void onNext(Measurements.MeasurementMessage value) {
            receivedMeasurement = new ImmutableMeasurement(value);
            synchronized (lock) {
                called = true;
                lock.notifyAll();
            }
        }

        @Override
        public void onError(Throwable t) {

        }

        @Override
        public void onCompleted() {

        }
    }
}
