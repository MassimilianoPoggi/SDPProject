package edu.di.unimi.it.sdp.project.edges.election;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.model.communications.Edges;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

public class CoordinatorElectionServiceTests {
    private static final int SERVER_PORT = 8080;
    private static Server server;
    private static ElectionManager electionManager;


    @BeforeClass
    public static void setUpServer() throws Exception {
        electionManager = mock(ElectionManager.class);

        server = ServerBuilder
                .forPort(SERVER_PORT)
                .addService(new CoordinatorElectionService(electionManager))
                .build();
        server.start();
    }

    @AfterClass
    public static void tearDownServer() {
        server.shutdown();
    }

    @Before
    public void resetMocks() {
        reset(electionManager);
    }

    @Test
    public void testStartElection() throws Exception {
        Edge edge = new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, 0);

        ManagedChannel channel =
                ManagedChannelBuilder
                        .forAddress("localhost", SERVER_PORT)
                        .usePlaintext()
                        .build();

        CoordinatorElectionGrpc.CoordinatorElectionBlockingStub stub = CoordinatorElectionGrpc.newBlockingStub(channel);

        Edges.EdgeMessage reply = stub.startElection(edge.toMessage());

        assertEquals(edge, new Edge(reply));
        verify(electionManager).startElection();
        channel.shutdown();
    }

    @Test
    public void testEndElection() throws Exception {
        Edge edge = new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, 0);

        ManagedChannel channel =
                ManagedChannelBuilder
                        .forAddress("localhost", SERVER_PORT)
                        .usePlaintext()
                        .build();

        CoordinatorElectionGrpc.CoordinatorElectionBlockingStub stub = CoordinatorElectionGrpc.newBlockingStub(channel);

        Edges.EdgeMessage reply = stub.nodeElected(edge.toMessage());

        assertEquals(edge, new Edge(reply));
        verify(electionManager).setCoordinator(edge);
        channel.shutdown();
    }
}
