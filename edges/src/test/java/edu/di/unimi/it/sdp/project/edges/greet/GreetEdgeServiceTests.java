package edu.di.unimi.it.sdp.project.edges.greet;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.model.communications.Edges;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class GreetEdgeServiceTests {
    private static final int SERVER_PORT = 8080;
    private static Server server;
    private static EdgesChannelsManager edgesChannelsManager;
    private static ElectionManager electionManager;


    @BeforeClass
    public static void setUpServer() throws Exception {
        edgesChannelsManager = mock(EdgesChannelsManager.class);
        electionManager = mock(ElectionManager.class);

        server = ServerBuilder
                .forPort(SERVER_PORT)
                .addService(new GreetEdgeService(edgesChannelsManager, electionManager))
                .build();
        server.start();
    }

    @AfterClass
    public static void tearDownServer() {
        server.shutdown();
    }

    @Before
    public void resetMocks() {
        reset(edgesChannelsManager, electionManager);
    }

    @Test
    public void testSuccessfulGreet() throws Exception {
        Edge edge = new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, 0);

        Edge coordinator = new Edge("Coordinator", new GridPosition(50, 50), InetAddress.getLocalHost(), 0, 0);

        when(electionManager.getCoordinator()).thenReturn(coordinator);

        ManagedChannel channel =
                ManagedChannelBuilder
                        .forAddress("localhost", SERVER_PORT)
                        .usePlaintext()
                        .build();

        GreetEdgeGrpc.GreetEdgeBlockingStub stub = GreetEdgeGrpc.newBlockingStub(channel);

        Edges.EdgeMessage reply = stub.greet(edge.toMessage());

        assertEquals(coordinator, new Edge(reply));
        verify(electionManager).getCoordinator();
        verifyNoMoreInteractions(electionManager);
        verify(edgesChannelsManager).addEdge(edge);
        verifyNoMoreInteractions(edgesChannelsManager);

        channel.shutdown();
    }
}
