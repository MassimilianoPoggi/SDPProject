package edu.di.unimi.it.sdp.project.edges.election;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.model.communications.Edges;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ElectionRunnableTests {
    private static final int SERVER_PORT = 8080;

    @Test
    public void testOtherEdgeElected() throws Exception {
        Server server = null;
        ManagedChannel channel = null;
        try {
            Edge thisEdge = getEdge("1", 0);
            Edge otherEdge = getEdge("2", SERVER_PORT);

            MockElectionService electionService = new MockElectionService();
            server = ServerBuilder.forPort(SERVER_PORT)
                    .addService(electionService)
                    .build();
            server.start();

            channel = ManagedChannelBuilder
                    .forAddress("localhost", SERVER_PORT)
                    .usePlaintext()
                    .build();

            EdgesChannelsManager edgesChannelsManager = mock(EdgesChannelsManager.class);
            when(edgesChannelsManager.getChannel(otherEdge)).thenReturn(channel);
            when(edgesChannelsManager.getEdges()).thenReturn(Arrays.asList(thisEdge, otherEdge));

            ElectionRunnable electionRunnable = new ElectionRunnable(thisEdge, edgesChannelsManager);

            Thread t = new Thread(electionRunnable);
            t.start();
            t.join();

            assertTrue(electionService.startCalled);
            assertFalse(electionService.endCalled);
        } finally {
            if (server != null) {
                server.shutdownNow();
                server.awaitTermination();
            }
            if (channel != null)
                channel.shutdownNow();
        }
    }

    @Test
    public void testThisEdgeElected() throws Exception {
        Server server = null;
        ManagedChannel channel = null;
        ManagedChannel unresponsiveChannel = null;
        try {
            Edge thisEdge = getEdge("2", SERVER_PORT);
            Edge otherEdge = getEdge("3", 0);

            MockElectionService electionService = new MockElectionService();
            server = ServerBuilder.forPort(SERVER_PORT)
                    .addService(electionService)
                    .build();
            server.start();

            channel = ManagedChannelBuilder
                    .forAddress("localhost", SERVER_PORT)
                    .usePlaintext()
                    .build();

            unresponsiveChannel = ManagedChannelBuilder
                    .forAddress("localhost", 0)
                    .usePlaintext()
                    .build();

            EdgesChannelsManager edgesChannelsManager = mock(EdgesChannelsManager.class);
            when(edgesChannelsManager.getChannel(otherEdge)).thenReturn(unresponsiveChannel);
            when(edgesChannelsManager.getChannel(thisEdge)).thenReturn(channel);
            when(edgesChannelsManager.getEdges()).thenReturn(Arrays.asList(thisEdge, otherEdge));

            ElectionRunnable electionRunnable = new ElectionRunnable(thisEdge, edgesChannelsManager);

            Thread t = new Thread(electionRunnable);
            t.start();
            t.join();

            assertFalse(electionService.startCalled);
            assertTrue(electionService.endCalled);
            assertEquals(thisEdge, electionService.electedEdge);
        } finally {
            if (server != null) {
                server.shutdownNow();
                server.awaitTermination();
            }
            if (channel != null)
                channel.shutdownNow();
            if (unresponsiveChannel != null)
                unresponsiveChannel.shutdownNow();
        }
    }

    public Edge getEdge(String id, int port) throws UnknownHostException {
        return new Edge(id, new GridPosition(0, 0), InetAddress.getLocalHost(), 0, port);
    }

    public class MockElectionService extends CoordinatorElectionGrpc.CoordinatorElectionImplBase {
        private boolean startCalled = false;
        private boolean endCalled = false;
        private Edge electedEdge = null;

        @Override
        public void startElection(Edges.EdgeMessage request, StreamObserver<Edges.EdgeMessage> responseObserver) {
            startCalled = true;
            responseObserver.onNext(request);
            responseObserver.onCompleted();
        }

        @Override
        public void nodeElected(Edges.EdgeMessage request, StreamObserver<Edges.EdgeMessage> responseObserver) {
            endCalled = true;
            try {
                electedEdge = new Edge(request);
            } catch (UnknownHostException e) {
                fail();
            }
            responseObserver.onNext(request);
            responseObserver.onCompleted();
        }
    }
}
