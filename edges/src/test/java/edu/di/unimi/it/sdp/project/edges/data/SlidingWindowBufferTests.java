package edu.di.unimi.it.sdp.project.edges.data;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.Calendar;
import java.util.stream.IntStream;

public class SlidingWindowBufferTests extends TestJPF {
    @Rule
    public Timeout timeout = Timeout.seconds(2);

    @Test
    public void testMeasurementAggregation() throws Exception {
        long startTime = millisecondsOfDay();

        SlidingWindowBuffer buffer = new SlidingWindowBuffer(getEdge("EdgeID"));
        double total = 0;
        for (int i = 0; i < 40; i++) {
            final int value = i;
            total += i;
            new Thread(() -> buffer.addMeasurement(getMeasurement(value))).start();
        }

        ImmutableMeasurement m = buffer.getAggregatedMeasurement();

        long endTime = millisecondsOfDay();
        assertEquals("EdgeID", m.getId());
        assertEquals("Type", m.getType());
        assertEquals(total / 40, m.getValue(), 0.0001);
        assertTrue(startTime <= m.getTimestamp() && m.getTimestamp() <= endTime);
    }

    @Test
    public void testConcurrentWrites() {
        if (verifyNoPropertyViolation()) {
            SlidingWindowBuffer buffer = new SlidingWindowBuffer(getEdge("EdgeID"));

            new Thread(() -> buffer.addMeasurement(getMeasurement(1))).start();
            new Thread(() -> buffer.addMeasurement(getMeasurement(1))).start();
        }
    }

    @Test
    public void testWindowOverlap() throws Exception {
        SlidingWindowBuffer buffer = new SlidingWindowBuffer(getEdge("EdgeID"));

        for (int i = 0; i < 60; i++)
            buffer.addMeasurement(getMeasurement(i));

        ImmutableMeasurement m1 = buffer.getAggregatedMeasurement();
        ImmutableMeasurement m2 = buffer.getAggregatedMeasurement();

        assertEquals("EdgeID", m1.getId());
        assertEquals("Type", m1.getType());
        assertEquals(IntStream.range(0, 40).average().getAsDouble(), m1.getValue(), 0.0001);

        assertEquals("EdgeID", m2.getId());
        assertEquals("Type", m2.getType());
        assertEquals(IntStream.range(20, 60).average().getAsDouble(), m2.getValue(), 0.0001);
    }

    private Edge getEdge(String id) {
        return new Edge(id, null, null, 0, 0);
    }

    private ImmutableMeasurement getMeasurement(double value) {
        return new ImmutableMeasurement("ID", "Type", value, 10);
    }

    private long millisecondsOfDay() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return (System.currentTimeMillis() - c.getTimeInMillis());
    }
}
