package edu.di.unimi.it.sdp.project.edges.grid;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.edges.RestInteractionTests;
import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.server.edges.EdgeManager;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class EdgeInsertionManagerTests extends RestInteractionTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws Exception {
        server = createServer(MockEdgeServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Before
    public void clearServer() {
        MockEdgeServer.callsNumber = 0;
        MockEdgeServer.receivedEdge = null;
        MockEdgeServer.edges = new ArrayList<>();
    }

    @Test
    public void testInsertEdge() throws Exception {
        EdgesChannelsManager edgesChannelsManager = mock(EdgesChannelsManager.class);

        Edge coordinator = getEdge("Coordinator");
        MockEdgeServer.edges.add(coordinator);

        Edge e = getEdge("validEdge");
        EdgeInsertionManager edgeInsertionManager = new EdgeInsertionManager(e, target, edgesChannelsManager);

        List<Edge> edges = edgeInsertionManager.insertIntoGrid();
        assertEquals(2, edges.size());
        assertTrue(edges.contains(e));
        assertTrue(edges.contains(coordinator));
        assertEquals(e.getID(), MockEdgeServer.receivedEdge.getID());
        assertEquals(e.getIpAddress(), MockEdgeServer.receivedEdge.getIpAddress());
        assertEquals(e.getSensorPort(), MockEdgeServer.receivedEdge.getSensorPort());
        assertEquals(e.getEdgePort(), MockEdgeServer.receivedEdge.getEdgePort());

        verify(edgesChannelsManager).addEdge(e);
        verify(edgesChannelsManager).addEdge(coordinator);
        verifyNoMoreInteractions(edgesChannelsManager);
    }

    @Test
    public void testInsertExistingEdge() throws Exception {
        Edge e = getEdge("invalidIdEdge");
        EdgeInsertionManager lifespanManager = new EdgeInsertionManager(e, target, null);

        assertEquals(null, lifespanManager.insertIntoGrid());
        assertEquals(1, MockEdgeServer.callsNumber);
    }

    @Test
    public void testInsertInvalidPosition() throws Exception {
        Edge e = getEdge("invalidPositionEdge");
        EdgeInsertionManager lifespanManager = new EdgeInsertionManager(e, target, null);

        assertEquals(null, lifespanManager.insertIntoGrid());
        assertEquals(10, MockEdgeServer.callsNumber);
    }

    private Edge getEdge(String id) throws Exception {
        return new Edge(id, new GridPosition(0, 0), InetAddress.getLocalHost(), 12345, 54321);
    }

    @Path("/edges")
    public static class MockEdgeServer {
        private static Edge receivedEdge;
        private static List<Edge> edges;
        private static int callsNumber = 0;

        @PUT
        @Consumes(MediaType.APPLICATION_JSON)
        @Produces(MediaType.APPLICATION_JSON)
        public Response addEdge(Edge e) {
            switch (e.getID()) {
                case "validEdge":
                    receivedEdge = e;
                    edges.add(e);
                    return Response.status(Response.Status.CREATED)
                            .entity(edges).build();
                case "invalidIdEdge":
                    callsNumber++;
                    return Response.status(Response.Status.CONFLICT)
                            .entity(EdgeManager.Conflict.EXISTING_ID).build();
                case "invalidPositionEdge":
                    callsNumber++;
                    return Response.status(Response.Status.CONFLICT)
                            .entity(EdgeManager.Conflict.TOO_CLOSE).build();
                default:
                    return Response.serverError().build();
            }
        }
    }
}
