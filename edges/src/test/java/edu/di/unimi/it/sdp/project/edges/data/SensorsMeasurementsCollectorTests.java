package edu.di.unimi.it.sdp.project.edges.data;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class SensorsMeasurementsCollectorTests {
    private static final int SERVER_PORT = 8080;
    private static Server server;
    private static SlidingWindowBuffer buffer;


    @BeforeClass
    public static void setUpServer() throws Exception {
        buffer = mock(SlidingWindowBuffer.class);

        server = ServerBuilder
                .forPort(SERVER_PORT)
                .addService(new SensorsMeasurementsCollector(buffer))
                .build();
        server.start();
    }

    @AfterClass
    public static void tearDownServer() {
        server.shutdown();
    }

    @Before
    public void resetMock() {
        reset(buffer);
    }

    @Test
    public void testSuccessfulReceive() throws Exception {
        ImmutableMeasurement measurement = new ImmutableMeasurement("ID", "Type", 5.0, 5);

        ManagedChannel channel =
                ManagedChannelBuilder
                        .forAddress("localhost", SERVER_PORT)
                        .usePlaintext()
                        .build();

        MeasurementsCollectorGrpc.MeasurementsCollectorStub stub = MeasurementsCollectorGrpc.newStub(channel);

        StreamObserver<Measurements.MeasurementMessage> streamObserver = stub.streamMeasurements(
                new StreamObserver<Measurements.MeasurementMessage>() {
                    @Override
                    public void onNext(Measurements.MeasurementMessage value) {
                        //edge sent a reply?
                        fail();
                    }

                    @Override
                    public void onError(Throwable t) {
                        //edge quit
                    }

                    @Override
                    public void onCompleted() {
                        //edge quit
                    }
                });

        streamObserver.onNext(measurement.toMessage());
        Thread.sleep(500);
        verify(buffer).addMeasurement(measurement);
        verifyNoMoreInteractions(buffer);
        channel.shutdownNow();
    }
}
