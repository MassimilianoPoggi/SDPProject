package edu.di.unimi.it.sdp.project.edges.greet;

import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.model.communications.Edges.EdgeMessage;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class GreetManagerTests {
    private final static int SERVER_PORT = 8080;

    @Test
    public void testGreetSending() throws Exception {
        Server server = null;
        try {
            Edge localhost = getLocalHostEdge(SERVER_PORT);
            MockGreetEdge greeter = new MockGreetEdge(localhost);
            server = ServerBuilder.forPort(SERVER_PORT)
                    .addService(greeter)
                    .build();
            server.start();

            Edge thisEdge = new Edge("MyID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, 0);
            ElectionManager electionManager = mock(ElectionManager.class);

            EdgesChannelsManager edgesChannelsManager = mock(EdgesChannelsManager.class);
            when(edgesChannelsManager.getChannel(localhost))
                    .thenReturn(ManagedChannelBuilder.forAddress("localhost", SERVER_PORT).usePlaintext().build());

            GreetManager greetManager = new GreetManager(thisEdge, Arrays.asList(localhost), edgesChannelsManager, electionManager);

            greetManager.greet();

            assertEquals(thisEdge, greeter.greeted);
            verify(edgesChannelsManager).getChannel(localhost);
            verifyNoMoreInteractions(edgesChannelsManager);
            verify(electionManager).setCoordinator(localhost);
            verify(electionManager).finishedJoining();
            verifyNoMoreInteractions(electionManager);
        } finally {
            if (server != null) {
                server.shutdown();
                server.awaitTermination();
            }
        }
    }

    @Test
    public void testUnreachableEdge() throws Exception {
        Edge localhost = getLocalHostEdge(0);
        List<Edge> edges = new ArrayList<>();
        edges.add(localhost);

        Edge thisEdge = new Edge("MyID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, 0);
        ElectionManager electionManager = mock(ElectionManager.class);

        EdgesChannelsManager edgesChannelsManager = mock(EdgesChannelsManager.class);
        when(edgesChannelsManager.getChannel(localhost))
                .thenReturn(ManagedChannelBuilder.forAddress("localhost", 0).usePlaintext().build());

        GreetManager greetManager = new GreetManager(thisEdge, Arrays.asList(localhost), edgesChannelsManager, electionManager);

        greetManager.greet();

        verify(edgesChannelsManager).getChannel(localhost);
        verify(edgesChannelsManager).removeEdge(localhost);
        verifyNoMoreInteractions(edgesChannelsManager);
        verify(electionManager).finishedJoining();
        verifyNoMoreInteractions(electionManager);
    }

    public Edge getLocalHostEdge(int port) throws UnknownHostException {
        return new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, port);
    }

    public class MockGreetEdge extends GreetEdgeGrpc.GreetEdgeImplBase {
        private Edge greeted;
        private Edge coordinator;

        public MockGreetEdge(Edge coordinator) {
            this.coordinator = coordinator;
        }

        @Override
        public void greet(EdgeMessage request, StreamObserver<EdgeMessage> responseObserver) {
            try {
                this.greeted = new Edge(request);
            } catch (UnknownHostException e) {
                this.greeted = null;
            }
            responseObserver.onNext(coordinator.toMessage());
            responseObserver.onCompleted();
        }
    }
}
