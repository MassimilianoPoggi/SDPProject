package edu.di.unimi.it.sdp.project.edges.grid;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.edges.RestInteractionTests;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.InetAddress;

import static junit.framework.TestCase.assertTrue;

public class EdgeRemovalManagerTests extends RestInteractionTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws Exception {
        server = createServer(MockEdgeServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testRemoveEdge() throws Exception {
        EdgeRemovalManager edgeRemovalManager = new EdgeRemovalManager(getEdge("validEdge"), target);

        edgeRemovalManager.removeFromGrid();

        assertTrue(MockEdgeServer.removalCalled);
    }

    private Edge getEdge(String id) throws Exception {
        return new Edge(id, new GridPosition(0, 0), InetAddress.getLocalHost(), 12345, 54321);
    }

    @Path("/edges")
    public static class MockEdgeServer {
        private static boolean removalCalled = false;

        @Path("validEdge")
        @DELETE
        public Response removeEdge() {
            removalCalled = true;
            return Response.ok().build();
        }
    }
}
