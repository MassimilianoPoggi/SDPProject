package edu.di.unimi.it.sdp.project.edges.coordinator;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.edges.RestInteractionTests;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ServerChannelTests extends RestInteractionTests {
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUpServer() throws IOException {
        server = createServer(MockMeasuresServer.class);
        server.start();
        target = getTargetToServer();
    }

    @AfterClass
    public static void closeServer() {
        if (server != null)
            server.stop(0);
    }

    @Test
    public void testSendMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID1", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID1", "Type", 10.0, 10);

        MeasurementsStore store = mock(MeasurementsStore.class);
        when(store.getNewMeasurements()).thenReturn(Arrays.asList(m1, m2));

        ServerChannel channel = new ServerChannel(target, store);
        new Thread(channel).start();

        synchronized (MockMeasuresServer.class) {
            MockMeasuresServer.class.wait();
        }

        ImmutableMeasurement lastGlobalMeasurement = channel.getLastGlobalMeasurement();
        channel.shutdown();

        assertEquals(MockMeasuresServer.globalMeasurement, lastGlobalMeasurement);
        assertEquals(7.5, lastGlobalMeasurement.getValue(), 0.0001);
        assertEquals(2, MockMeasuresServer.edgeMeasurements.size());
        assertTrue(MockMeasuresServer.edgeMeasurements.contains(m1));
        assertTrue(MockMeasuresServer.edgeMeasurements.contains(m2));
    }

    @Path("/stats/measures")
    public static class MockMeasuresServer {
        private static List<ImmutableMeasurement> edgeMeasurements = new ArrayList<>();
        private static ImmutableMeasurement globalMeasurement;
        private static int called = 0;

        @PUT
        @Path("/edge/ID1")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response addEdgeMeasurement(ImmutableMeasurement m) {
            synchronized (MockMeasuresServer.class) {
                edgeMeasurements.add(m);
                called++;
                if (called == 3)
                    MockMeasuresServer.class.notifyAll();
            }
            return Response.ok().build();
        }

        @PUT
        @Path("/global")
        @Consumes(MediaType.APPLICATION_JSON)
        public Response addGlobalMeasurement(ImmutableMeasurement m) {
            synchronized (MockMeasuresServer.class) {
                globalMeasurement = m;
                called++;
                if (called == 3)
                    MockMeasuresServer.class.notifyAll();
            }
            return Response.ok().build();
        }
    }
}
