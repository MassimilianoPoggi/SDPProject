package edu.di.unimi.it.sdp.project.edges.data;

import edu.di.unimi.it.sdp.project.edges.EdgePanel;
import edu.di.unimi.it.sdp.project.edges.channels.EdgesChannelsManager;
import edu.di.unimi.it.sdp.project.edges.election.ElectionManager;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.Before;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CoordinatorChannelTests {
    private static final int SERVER_PORT = 8080;

    private ElectionManager electionManager = mock(ElectionManager.class);
    private EdgesChannelsManager edgesChannelsManager = mock(EdgesChannelsManager.class);
    private SlidingWindowBuffer slidingWindowBuffer = mock(SlidingWindowBuffer.class);
    private EdgePanel edgePanel = mock(EdgePanel.class);

    @Before
    public void resetMocks() {
        reset(electionManager, edgesChannelsManager, slidingWindowBuffer, edgePanel);
    }

    @Test
    public void testMeasurementSending() throws Exception {
        Server server = null;
        ManagedChannel channel = null;
        try {
            ImmutableMeasurement m = new ImmutableMeasurement("ID", "Type", 10.0, 10);

            MockMeasurementsCollector collector = new MockMeasurementsCollector();
            server = ServerBuilder.forPort(SERVER_PORT)
                    .addService(collector)
                    .build();
            server.start();

            Edge coordinator = getLocalHostEdge(SERVER_PORT);
            when(electionManager.getCoordinator()).thenReturn(coordinator);

            when(slidingWindowBuffer.getAggregatedMeasurement()).thenReturn(m);

            channel = ManagedChannelBuilder
                    .forAddress("localhost", SERVER_PORT)
                    .usePlaintext()
                    .build();

            when(edgesChannelsManager.getChannel(coordinator)).thenReturn(channel);

            CoordinatorChannel coordinatorChannel = new CoordinatorChannel(edgesChannelsManager, slidingWindowBuffer,
                    electionManager, edgePanel);

            coordinatorChannel.start();

            synchronized (collector.globalMeasurement) {
                while (!collector.called)
                    collector.globalMeasurement.wait();
            }

            coordinatorChannel.close();

            Thread.sleep(1000);

            assertEquals(m, collector.receivedMeasurement);
            verify(edgePanel, atLeastOnce()).receivedLocalMeasurement(m);
            verify(edgePanel, atLeastOnce()).receivedGlobalMeasurement(collector.globalMeasurement);
        } finally {
            if (server != null) {
                server.shutdownNow();
                server.awaitTermination();
            }
            if (channel != null)
                channel.shutdownNow();
        }
    }

    @Test
    public void testUnreachableCoordinator() throws Exception {
        ImmutableMeasurement m = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        Edge coordinator = getLocalHostEdge(SERVER_PORT);
        when(electionManager.getCoordinator()).thenReturn(coordinator).thenReturn(null);

        when(slidingWindowBuffer.getAggregatedMeasurement()).thenReturn(m);

        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", SERVER_PORT)
                .usePlaintext()
                .build();
        when(edgesChannelsManager.getChannel(coordinator)).thenReturn(channel);

        CoordinatorChannel coordinatorChannel = new CoordinatorChannel(edgesChannelsManager, slidingWindowBuffer,
                electionManager, edgePanel);

        coordinatorChannel.start();

        Thread.sleep(1000);

        verify(edgesChannelsManager).getChannel(coordinator);
        verify(edgesChannelsManager).removeEdge(coordinator);
        verify(electionManager).startElection();
        verify(edgePanel, atLeastOnce()).receivedLocalMeasurement(m);

        coordinatorChannel.close();
        channel.shutdownNow();
    }

    public Edge getLocalHostEdge(int port) throws UnknownHostException {
        return new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, port);
    }

    private class MockMeasurementsCollector extends MeasurementsCollectorGrpc.MeasurementsCollectorImplBase {
        private ImmutableMeasurement receivedMeasurement;
        private ImmutableMeasurement globalMeasurement = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);
        private boolean called = false;

        @Override
        public StreamObserver<Measurements.MeasurementMessage> streamMeasurements(
                StreamObserver<Measurements.MeasurementMessage> responseObserver) {
            return new StreamObserver<Measurements.MeasurementMessage>() {
                @Override
                public void onNext(Measurements.MeasurementMessage value) {
                    receivedMeasurement = new ImmutableMeasurement(value);
                    responseObserver.onNext(globalMeasurement.toMessage());
                    synchronized (globalMeasurement) {
                        called = true;
                        globalMeasurement.notifyAll();
                    }
                }

                @Override
                public void onError(Throwable t) {
                    //edge quit
                }

                @Override
                public void onCompleted() {
                    //edge quit
                }
            };
        }
    }
}
