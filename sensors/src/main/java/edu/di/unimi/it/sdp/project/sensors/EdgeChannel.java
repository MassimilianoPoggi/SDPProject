package edu.di.unimi.it.sdp.project.sensors;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.communications.Measurements.MeasurementMessage;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import edu.di.unimi.it.sdp.project.sensors.simulator.Measurement;
import edu.di.unimi.it.sdp.project.sensors.simulator.SensorStream;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.logging.Logger;

public class EdgeChannel implements SensorStream {
    private final NearestFinderThread finderThread;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    private Edge channelEdge;
    private ManagedChannel channel;
    private StreamObserver<MeasurementMessage> streamObserver;

    public EdgeChannel(NearestFinderThread finderThread) {
        this.finderThread = finderThread;
        this.channel = null;
        this.channelEdge = null;
        this.streamObserver = null;
    }

    @Override
    public void sendMeasurement(Measurement m) {
        updateStreamObserver();
        synchronized (finderThread) {
            if (streamObserver != null)
                streamObserver.onNext(fromMeasurement(m));
            else {
                logger.info("No nearest edge while sending measurement: asking for an update");
                finderThread.interrupt();
            }
        }
    }

    private void updateStreamObserver() {
        Edge nearest = finderThread.getNearestEdge();
        if (nearest != null && (channelEdge == null || !channelEdge.equals(nearest))) {
            channel = ManagedChannelBuilder
                    .forAddress(nearest.getIpAddress().getHostAddress(), nearest.getSensorPort())
                    .usePlaintext()
                    .build();
            channelEdge = nearest;

            logger.info("Opened channel " + channel + " to new nearest edge " + channelEdge);

            streamObserver = MeasurementsCollectorGrpc.newStub(channel).streamMeasurements(
                    new StreamObserver<MeasurementMessage>() {
                        @Override
                        public void onNext(MeasurementMessage value) {
                            logger.severe("Received unexpected reply from edge");
                        }

                        @Override
                        public void onError(Throwable t) {
                            onEdgeQuit();
                        }

                        @Override
                        public void onCompleted() {
                            onEdgeQuit();
                        }

                        private void onEdgeQuit() {
                            synchronized(finderThread) {
                                channelEdge = null;
                                channel.shutdownNow();
                                streamObserver = null;
                                finderThread.interrupt();
                            }
                        }
                    });
        }
    }

    private MeasurementMessage fromMeasurement(Measurement m) {
        return MeasurementMessage.newBuilder()
                .setId(m.getId())
                .setType(m.getType())
                .setValue(m.getValue())
                .setTimestamp(m.getTimestamp())
                .build();
    }
}
