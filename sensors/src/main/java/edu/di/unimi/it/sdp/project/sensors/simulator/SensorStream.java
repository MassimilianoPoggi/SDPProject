package edu.di.unimi.it.sdp.project.sensors.simulator;

public interface SensorStream {

    void sendMeasurement(Measurement m);

}
