package edu.di.unimi.it.sdp.project.sensors;

import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.sensors.simulator.PM10Simulator;
import edu.di.unimi.it.sdp.project.sensors.simulator.SensorStream;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Logger;

public class SensorGenerator {
    public static final Logger LOGGER = Logger.getLogger(SensorGenerator.class.getName());
    private static PM10Simulator simulators[];
    private static NearestFinderThread threads[];

    public static void main(String... args) {
        if (args.length < 2) {
            System.out.println("Please insert server address and number of sensors to generate.");
            return;
        }

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://" + args[0] + "/server/rest");

        int sensors = Integer.parseInt(args[1]);
        simulators = new PM10Simulator[sensors];
        threads = new NearestFinderThread[sensors];
        for (int i = 0; i < sensors; i++)
            startSensor(i, target);

        Scanner sc = new Scanner(new InputStreamReader(System.in));
        while (!sc.nextLine().equals("exit"))
            System.out.println("Type exit to exit");

        for (PM10Simulator s : simulators)
            s.stopMeGently();
        for (NearestFinderThread f : threads) {
            f.shutdown();
            try {
                f.join();
            } catch (InterruptedException e) {
                LOGGER.severe("Thread shouldn't get interrupted while waiting for sensors to shut down");
            }
        }
    }

    private static void startSensor(int index, WebTarget target) {
        threads[index] = new NearestFinderThread(target, GridPosition.random());
        threads[index].start();
        SensorStream stream = new EdgeChannel(threads[index]);
        simulators[index] = new PM10Simulator(stream);
        new Thread(simulators[index]).start();
    }
}
