package edu.di.unimi.it.sdp.project.sensors;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.Closeable;
import java.io.IOException;
import java.util.logging.Logger;

public class NearestFinderThread extends Thread {
    private static final int POLLING_INTERVAL = 10000;

    private final WebTarget target;
    private final GridPosition position;
    private final Logger logger = Logger.getLogger(this.getClass().getName());
    private volatile boolean shutdown;
    private Edge nearestEdge;

    public NearestFinderThread(WebTarget target, GridPosition position) {
        this.target = target;
        this.position = position;
        this.shutdown = false;
        this.nearestEdge = null;
    }

    @Override
    public void run() {
        while (!shutdown) {
            try {
                Response response = target
                        .path("edges")
                        .path("nearest")
                        .path(position.toString())
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

                switch (response.getStatusInfo().toEnum()) {
                    case OK:
                        Edge nearest = response.readEntity(Edge.class);
                        logger.info("Edge " + nearest + " was returned as edge nearest to " + position);
                        synchronized (this) {
                            nearestEdge = nearest;
                        }
                        break;
                    case NOT_FOUND:
                        logger.info("Server returned that there are no edges in the grid");
                        break;
                    default:
                        logger.severe("Unexpected reply from server");
                        break;
                }
            } catch (Exception e) {
                logger.warning("Server isn't responding. Is the server address correct?");
            }

            try {
                Thread.sleep(POLLING_INTERVAL);
            } catch (InterruptedException e) {
                logger.info("Update for nearest edge was requested");
            }
        }
    }

    public synchronized Edge getNearestEdge() {
        return nearestEdge;
    }

    public void shutdown() {
        this.shutdown = true;
        this.interrupt();
    }
}
