package edu.di.unimi.it.sdp.project.sensors;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.model.communications.Measurements;
import edu.di.unimi.it.sdp.project.model.communications.MeasurementsCollectorGrpc;
import edu.di.unimi.it.sdp.project.sensors.simulator.Measurement;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.junit.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class EdgeChannelTests {
    @Test
    public void testMeasurementSending() throws Exception {
        Server server = null;
        try {
            Measurement m = getTestMeasurement();

            MockMeasurementsCollector collector = new MockMeasurementsCollector(m);
            server = ServerBuilder.forPort(0)
                    .addService(collector)
                    .build();
            server.start();

            NearestFinderThread t = mock(NearestFinderThread.class);
            Edge localhost = getLocalHostEdge(server.getPort());
            when(t.getNearestEdge()).thenReturn(localhost);

            EdgeChannel channel = new EdgeChannel(t);
            channel.sendMeasurement(m);

            synchronized(m) {
                if (!collector.called)
                    m.wait();
            }

            assertTrue(measurementEquals(m, collector.observedMeasurement));
            verify(t).getNearestEdge();
            verifyNoMoreInteractions(t);
        } finally {
            if (server != null) {
                server.shutdownNow();
                server.awaitTermination();
            }
        }
    }

    @Test
    public void testNoNearestEdge() {
        NearestFinderThread t = mock(NearestFinderThread.class);
        when(t.getNearestEdge()).thenReturn(null);

        Measurement m = getTestMeasurement();
        EdgeChannel channel = new EdgeChannel(t);

        channel.sendMeasurement(m);

        verify(t).getNearestEdge();
        verify(t).interrupt();
        verifyNoMoreInteractions(t);
    }

    public Edge getLocalHostEdge(int port) throws UnknownHostException {
        return new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), port, 0);
    }

    private Measurement getTestMeasurement() {
        return new Measurement("ID", "Type", 10.0, 10);
    }

    private boolean measurementEquals(Measurement m1, Measurement m2) {
        return m1.getId().equals(m2.getId()) &&
                m1.getType().equals(m2.getType()) &&
                m1.getValue() == m2.getValue() &&
                m1.getTimestamp() == m2.getTimestamp();
    }

    private class MockMeasurementsCollector extends MeasurementsCollectorGrpc.MeasurementsCollectorImplBase {
        private final Object lock;
        private Measurement observedMeasurement;
        private boolean called = false;

        public MockMeasurementsCollector(Object lock) {
            this.lock = lock;
        }

        @Override
        public StreamObserver<Measurements.MeasurementMessage> streamMeasurements(
                StreamObserver<Measurements.MeasurementMessage> responseObserver) {
            return new StreamObserver<Measurements.MeasurementMessage>() {
                @Override
                public void onNext(Measurements.MeasurementMessage value) {
                    observedMeasurement = new Measurement(
                            value.getId(),
                            value.getType(),
                            value.getValue(),
                            value.getTimestamp()
                    );
                    synchronized (lock) {
                        called = true;
                        lock.notifyAll();
                    }
                }

                @Override
                public void onError(Throwable t) {
                    //sensor quit
                }

                @Override
                public void onCompleted() {
                    //sensor quit
                }
            };
        }
    }
}
