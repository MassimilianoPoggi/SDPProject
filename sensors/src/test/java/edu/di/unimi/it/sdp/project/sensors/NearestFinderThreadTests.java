package edu.di.unimi.it.sdp.project.sensors;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.RuntimeDelegate;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class NearestFinderThreadTests {
    private static final int SERVER_PORT = 8080;

    @Test
    public void testNearest() throws Exception {
        HttpServer server = null;
        NearestFinderThread thread = null;
        try {
            MockNearestServer.edge = getEdge();

            server = createServer(MockNearestServer.class);
            server.start();

            WebTarget target = getTargetToServer();
            thread = new NearestFinderThread(target, new GridPosition(0, 0));

            thread.start();
            Thread.sleep(1000);
            assertEquals(MockNearestServer.edge, thread.getNearestEdge());
        } finally {
            if (server != null)
                server.stop(0);
            if (thread != null)
                thread.shutdown();
        }
    }

    private Edge getEdge() throws UnknownHostException {
        return new Edge("ID", new GridPosition(0, 0), InetAddress.getLocalHost(), 0, 0);
    }

    private HttpServer createServer(Class<?>... endpoints) throws IOException {
        URI uri = UriBuilder.fromUri("http://localhost/").port(SERVER_PORT).build();
        HttpServer server = HttpServer.create(new InetSocketAddress(uri.getPort()), 0);
        HttpHandler handler = RuntimeDelegate.getInstance()
                .createEndpoint(new ApplicationConfig(endpoints), HttpHandler.class);
        server.createContext(uri.getPath(), handler);
        return server;
    }

    private WebTarget getTargetToServer() {
        Client client = ClientBuilder.newClient();
        return client.target("http://localhost:" + SERVER_PORT + "/server/rest");
    }

    @Path("/server/rest/edges")
    public static class MockNearestServer {
        private static Edge edge;

        @GET
        @Path("/nearest/0x0")
        @Produces(MediaType.APPLICATION_JSON)
        public Response getNearest() {
            return Response.ok(edge).build();
        }
    }

    private class ApplicationConfig extends Application {
        Set<Class<?>> classes;

        ApplicationConfig(Class<?>... classes) {
            Set<Class<?>> cls = new HashSet<>(Arrays.asList(classes));
            this.classes = Collections.unmodifiableSet(cls);
        }

        @Override
        public Set<Class<?>> getClasses() {
            return classes;
        }
    }
}
