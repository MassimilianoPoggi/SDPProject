package edu.di.unimi.it.sdp.project.server.rest;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.server.edges.EdgeManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/edges")
public class EdgesResource {
    private final EdgeManager edgeManager = EdgeManager.INSTANCE;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @GET
    @Path("/nearest/{x}x{y}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getNearestEdge(@PathParam("x") int x, @PathParam("y") int y) {
        try {
            GridPosition position = new GridPosition(x, y);
            Edge edge = edgeManager.nearest(position);
            logger.info("Returned edge " + edge + " as nearest to " + position);
            return Response.ok(edge).build();
        } catch (IllegalArgumentException e) { //invalid position
            logger.info("An invalid position was received while asked for nearest node");
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (UnsupportedOperationException e) { //no edges in the grid
            logger.info("Nearest node was requested but there are no edges in the grid");
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEdges() {
        try {
            Response response = Response.ok(edgeManager.getEdges()).build();
            logger.info("Returned list of current edges");
            return response;
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addEdge(Edge edge) {
        try {
            Response response = Response.status(Response.Status.CREATED).entity(edgeManager.add(edge)).build();
            logger.info("Edge " + edge + " added to the grid");
            return response;
        } catch (IllegalStateException e) { //existing id
            logger.info("Edge " + edge + " tried to enter but ID already exists");
            return Response.status(Response.Status.CONFLICT).entity(EdgeManager.Conflict.EXISTING_ID).build();
        } catch (IllegalArgumentException e) { //nearby edge
            logger.info("Edge " + edge + " tried to enter but there's another edge too close");
            return Response.status(Response.Status.CONFLICT).entity(EdgeManager.Conflict.TOO_CLOSE).build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @DELETE
    @Path("/{id}")
    public Response removeEdge(@PathParam("id") String id) {
        try {
            edgeManager.remove(id);
            logger.info("Edge + " + id + " removed from the grid");
            return Response.ok().build();
        } catch (IllegalArgumentException e) { //invalid id
            logger.info("Invalid ID " + id + " was given while asking for edge removal");
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    private Response interruptedExceptionResponse() {
        logger.severe("Thread shouldn't ever get interrupted.");
        return Response.serverError().build();
    }
}
