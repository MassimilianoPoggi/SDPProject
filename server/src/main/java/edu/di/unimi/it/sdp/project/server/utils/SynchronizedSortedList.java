package edu.di.unimi.it.sdp.project.server.utils;

import edu.di.unimi.it.sdp.project.server.utils.synchronization.ReadLock;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.ReadWriteLock;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.WriteLock;

import java.util.ArrayList;
import java.util.List;

public class SynchronizedSortedList<T extends Comparable<? super T>> {
    private final ArrayList<T> list = new ArrayList<>();
    private final ReadWriteLock listLock = new ReadWriteLock();

    public boolean add(T t) throws InterruptedException {
        try (WriteLock lock = listLock.lockWrite()) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).compareTo(t) <= 0) {
                    list.add(i, t);
                    return true;
                }
            }

            return list.add(t);
        }
    }

    public List<T> subList(int i, int i1) throws InterruptedException {
        try (ReadLock lock = listLock.lockRead()) {
            return list.subList(i, i1);
        }
    }

    public int size() throws InterruptedException {
        try (ReadLock lock = listLock.lockRead()) {
            return list.size();
        }
    }

    public void clear() throws InterruptedException {
        try (WriteLock lock = listLock.lockWrite()) {
            list.clear();
        }
    }
}
