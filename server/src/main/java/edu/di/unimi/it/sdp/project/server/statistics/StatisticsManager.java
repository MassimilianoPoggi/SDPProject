package edu.di.unimi.it.sdp.project.server.statistics;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.server.utils.SynchronizedSortedList;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.ReadLock;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.ReadWriteLock;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.WriteLock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum StatisticsManager {
    INSTANCE;

    private final Map<String, SynchronizedSortedList<ImmutableMeasurement>> measurements;
    private final ReadWriteLock measurementsMapLock;

    private final SynchronizedSortedList<ImmutableMeasurement> globalMeasurements;
    private final SynchronizedSortedList<ImmutableMeasurement> localMeasurements;

    StatisticsManager() {
        measurements = new HashMap<>();
        globalMeasurements = new SynchronizedSortedList<>();
        localMeasurements = new SynchronizedSortedList<>();
        measurementsMapLock = new ReadWriteLock();
    }

    public List<ImmutableMeasurement> getGlobalMeasurements(int n) throws InterruptedException {
        return globalMeasurements.subList(0, Math.min(globalMeasurements.size(), n));
    }

    public List<ImmutableMeasurement> getEdgeMeasurements(String id, int n) throws InterruptedException {
        SynchronizedSortedList<ImmutableMeasurement> edgeMeasurements;

        try (ReadLock lock = measurementsMapLock.lockRead()) {
            if (!measurements.containsKey(id))
                throw new IllegalArgumentException("Invalid edge");

            edgeMeasurements = measurements.get(id);
        }

        return edgeMeasurements.subList(0, Math.min(edgeMeasurements.size(), n));
    }

    public List<ImmutableMeasurement> getLocalMeasurements(int n) throws InterruptedException {
        return localMeasurements.subList(0, Math.min(localMeasurements.size(), n));
    }

    public void addEdgeMeasurement(String id, ImmutableMeasurement measurement) throws InterruptedException {
        SynchronizedSortedList<ImmutableMeasurement> edgeMeasurements;

        try (WriteLock lock = measurementsMapLock.lockWrite()) {
            if (!measurements.containsKey(id))
                measurements.put(id, new SynchronizedSortedList<>());

            edgeMeasurements = measurements.get(id);
        }

        edgeMeasurements.add(measurement);
        localMeasurements.add(measurement);
    }

    public void addGlobalMeasurement(ImmutableMeasurement measurement) throws InterruptedException {
        globalMeasurements.add(measurement);
    }

    private double mean(List<ImmutableMeasurement> measurements) {
        return measurements.stream()
                .mapToDouble(ImmutableMeasurement::getValue)
                .sum()
                / measurements.size();
    }

    public double globalMean(int n) throws InterruptedException {
        return mean(getGlobalMeasurements(n));
    }

    public double edgeMean(String id, int n) throws InterruptedException {
        return mean(getEdgeMeasurements(id, n));
    }

    private double stdDev(List<ImmutableMeasurement> measurements) {
        double mean = mean(measurements);
        return Math.sqrt(
                measurements.stream()
                        .mapToDouble(m -> Math.pow(m.getValue() - mean, 2))
                        .sum()
                        / measurements.size());
    }

    public double globalStdDev(int n) throws InterruptedException {
        return stdDev(getGlobalMeasurements(n));
    }

    public double edgeStdDev(String id, int n) throws InterruptedException {
        return stdDev(getEdgeMeasurements(id, n));
    }

    public void clearAll() throws InterruptedException {
        try (WriteLock lock = measurementsMapLock.lockWrite()) {
            measurements.clear();
        }
        globalMeasurements.clear();
        localMeasurements.clear();
    }
}