package edu.di.unimi.it.sdp.project.server.utils.synchronization;

public class WriteLock implements AutoCloseable {
    private final ReadWriteLock rwLock;

    public WriteLock(ReadWriteLock rwLock) {
        this.rwLock = rwLock;
    }

    @Override
    public void close() {
        rwLock.unlockWrite();
    }
}
