package edu.di.unimi.it.sdp.project.server.rest;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.server.statistics.StatisticsManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Logger;

@Path("/stats")
public class StatisticsResource {
    private final StatisticsManager statisticsManager = StatisticsManager.INSTANCE;
    private final Logger logger = Logger.getLogger(this.getClass().getName());

    @GET
    @Path("/measures/global/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGlobalMeasurements(@PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.getGlobalMeasurements(n)).build();
            logger.info("Returned last " + n + " global measures");
            return response;
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @PUT
    @Path("/measures/global/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addGlobalMeasurement(ImmutableMeasurement measurement) {
        try {
            statisticsManager.addGlobalMeasurement(measurement);
            logger.info("Added " + measurement + " to global measures");
            return Response.ok().build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @PUT
    @Path("/measures/edge/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addEdgeMeasurement(@PathParam("id") String id, ImmutableMeasurement measurement) {
        try {
            statisticsManager.addEdgeMeasurement(id, measurement);
            logger.info("Added " + measurement + " to measures for edge " + id);
            return Response.ok().build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }


    @GET
    @Path("/measures/local/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLocalMeasurements(@PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.getLocalMeasurements(n)).build();
            logger.info("Returned last " + n + " local measures");
            return response;
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @GET
    @Path("/measures/edge/{id}/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEdgeMeasurements(@PathParam("id") String id, @PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.getEdgeMeasurements(id, n)).build();
            logger.info("Returned last " + n + " measures for edge " + id);
            return response;
        } catch (IllegalArgumentException e) {
            logger.info("Invalid ID " + id + " was given while asking for last " + n + " measures");
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @GET
    @Path("/stddev/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGlobalStdDev(@PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.globalStdDev(n)).build();
            logger.info("Returned standard deviation for the last " + n + " global measures");
            return response;
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @GET
    @Path("/stddev/{id}/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEdgeStdDev(@PathParam("id") String id, @PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.edgeStdDev(id, n)).build();
            logger.info("Returned standard deviation for the last " + n + " measures for edge " + id);
            return response;
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @GET
    @Path("/mean/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGlobalMean(@PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.globalMean(n)).build();
            logger.info("Returned mean for the last " + n + " global measures");
            return response;
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    @GET
    @Path("/mean/{id}/{n}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEdgeMean(@PathParam("id") String id, @PathParam("n") int n) {
        try {
            Response response = Response.ok(statisticsManager.edgeMean(id, n)).build();
            logger.info("Returned mean for the last " + n + " measures for edge " + id);
            return response;
        } catch (IllegalArgumentException e) {
            logger.info("Invalid ID " + id + " was given while asking for mean for the last " + n + " measures");
            return Response.status(Response.Status.NOT_FOUND).build();
        } catch (InterruptedException e) {
            return interruptedExceptionResponse();
        }
    }

    private Response interruptedExceptionResponse() {
        logger.severe("Thread shouldn't ever get interrupted");
        return Response.serverError().build();
    }
}
