package edu.di.unimi.it.sdp.project.server.utils.synchronization;

public class ReadLock implements AutoCloseable {
    private final ReadWriteLock rwLock;

    public ReadLock(ReadWriteLock rwLock) {
        this.rwLock = rwLock;
    }

    @Override
    public void close() {
        this.rwLock.unlockRead();
    }
}
