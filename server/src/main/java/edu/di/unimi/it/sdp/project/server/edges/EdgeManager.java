package edu.di.unimi.it.sdp.project.server.edges;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.ReadLock;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.ReadWriteLock;
import edu.di.unimi.it.sdp.project.server.utils.synchronization.WriteLock;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public enum EdgeManager {
    INSTANCE;

    private static final int MIN_DISTANCE = 20;
    private final Map<String, Edge> edges = new HashMap<>();
    private final ReadWriteLock edgeLock = new ReadWriteLock();

    public Collection<Edge> add(Edge edge) throws InterruptedException {
        try (WriteLock lock = edgeLock.lockWrite()) {
            if (edges.containsKey(edge.getID()))
                throw new IllegalStateException("ID already exists");

            if (edges.values().stream()
                    .anyMatch(e ->
                            e.getPosition().distance(edge.getPosition())
                                    < MIN_DISTANCE))
                throw new IllegalArgumentException("Another edge is too close");

            edges.put(edge.getID(), edge);
            return new ArrayList<>(edges.values());
        }
    }

    public void remove(String id) throws InterruptedException {
        try (WriteLock lock = edgeLock.lockWrite()) {
            if (!edges.containsKey(id))
                throw new IllegalArgumentException("Invalid ID");

            edges.remove(id);
        }
    }

    public Edge nearest(GridPosition position) throws InterruptedException {
        try (ReadLock lock = edgeLock.lockRead()) {
            return edges.values().stream()
                    .min(Comparator.comparingInt(
                            node -> node.getPosition().distance(position)))
                    .orElseThrow(()
                            -> new UnsupportedOperationException("No channels present"));
        }
    }

    public Collection<Edge> getEdges() throws InterruptedException {
        try (ReadLock lock = edgeLock.lockRead()) {
            return new ArrayList<>(edges.values());
        }
    }

    public void clear() throws InterruptedException {
        try (WriteLock lock = edgeLock.lockWrite()) {
            edges.clear();
        }
    }

    @XmlRootElement
    public enum Conflict {
        EXISTING_ID,
        TOO_CLOSE
    }
}

