package edu.di.unimi.it.sdp.project.server.utils.synchronization;

public class ReadWriteLock {
    public static final int WRITER_PRIORITIZATION_THRESHOLD = 5;

    private int readers;
    private boolean isWriting;
    private int waitingWriters;

    public ReadWriteLock() {
        this.readers = 0;
        this.isWriting = false;
        this.waitingWriters = 0;
    }

    public synchronized ReadLock lockRead() throws InterruptedException {
        while (isWriting || waitingWriters >= WRITER_PRIORITIZATION_THRESHOLD)
            wait();

        readers++;
        return new ReadLock(this);
    }

    public synchronized void unlockRead() {
        if (--readers == 0)
            notifyAll();
    }

    public synchronized WriteLock lockWrite() throws InterruptedException {
        waitingWriters++;
        while (isWriting || readers > 0)
            wait();

        waitingWriters--;
        isWriting = true;
        return new WriteLock(this);
    }

    public synchronized void unlockWrite() {
        isWriting = false;
        notifyAll();
    }
}
