package edu.di.unimi.it.sdp.project.server.rest;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import edu.di.unimi.it.sdp.project.server.statistics.StatisticsManager;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class StatisticsResourceTests extends RestServerTests {
    private static Client client;
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUp() throws Exception {
        client = ClientBuilder.newClient();
        server = createServer(StatisticsResource.class);
        target = client.target(UriBuilder.fromUri("http://localhost:" + SERVER_PORT));
        server.start();
    }

    @AfterClass
    public static void tearDown() {
        client.close();
        server.stop(0);
    }

    @Before
    public void clearEdges() throws Exception {
        StatisticsManager.INSTANCE.clearAll();
    }

    @Test
    public void testAddGlobalMeasurement() throws Exception {
        ImmutableMeasurement m = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        Response response =
                target.path("stats").path("measures").path("global")
                        .request()
                        .put(Entity.json(m));

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(Arrays.asList(m), StatisticsManager.INSTANCE.getGlobalMeasurements(1));
    }

    @Test
    public void testAddEdgeMeasurement() throws Exception {
        ImmutableMeasurement m = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        Response response =
                target.path("stats").path("measures").path("edge").path("ID")
                        .request()
                        .put(Entity.json(m));

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(Arrays.asList(m), StatisticsManager.INSTANCE.getEdgeMeasurements("ID", 1));
    }

    @Test
    public void testGetGlobalMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
        StatisticsManager.INSTANCE.addGlobalMeasurement(m2);

        Response response =
                target.path("stats").path("measures").path("global").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(Arrays.asList(m2, m1), response.readEntity(new GenericType<List<ImmutableMeasurement>>() {
        }));
    }

    @Test
    public void testGetEdgeMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        Response response =
                target.path("stats").path("measures").path("edge").path("ID").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(Arrays.asList(m2, m1), response.readEntity(new GenericType<List<ImmutableMeasurement>>() {
        }));
    }

    @Test
    public void testGetNonexistentEdgeMeasurements() {
        Response response =
                target.path("stats").path("measures").path("edge").path("ID").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.NOT_FOUND, response.getStatusInfo().toEnum());
    }

    @Test
    public void testGetLocalMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID1", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID2", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        Response response =
                target.path("stats").path("measures").path("local").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(Arrays.asList(m2, m1), response.readEntity(new GenericType<List<ImmutableMeasurement>>() {
        }));
    }

    @Test
    public void testGetGlobalMean() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
        StatisticsManager.INSTANCE.addGlobalMeasurement(m2);

        Response response =
                target.path("stats").path("mean").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(7.5, response.readEntity(Double.class), 0.0001);
    }

    @Test
    public void testGetEdgeMean() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        Response response =
                target.path("stats").path("mean").path("ID").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(7.5, response.readEntity(Double.class), 0.0001);
    }

    @Test
    public void testGetNonexistentEdgeMean() throws Exception {
        Response response =
                target.path("stats").path("mean").path("ID").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.NOT_FOUND, response.getStatusInfo().toEnum());
    }

    @Test
    public void testGetGlobalStdDev() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
        StatisticsManager.INSTANCE.addGlobalMeasurement(m2);

        Response response =
                target.path("stats").path("stddev").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(2.5, response.readEntity(Double.class), 0.0001);
    }

    @Test
    public void testGetEdgeStdDev() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        Response response =
                target.path("stats").path("stddev").path("ID").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(2.5, response.readEntity(Double.class), 0.0001);
    }

    @Test
    public void testGetNonexistentEdgeStdDev() throws Exception {
        Response response =
                target.path("stats").path("stddev").path("ID").path("2")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.NOT_FOUND, response.getStatusInfo().toEnum());
    }
}
