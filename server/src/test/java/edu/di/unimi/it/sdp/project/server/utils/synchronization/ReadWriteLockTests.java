package edu.di.unimi.it.sdp.project.server.utils.synchronization;

import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Test;

public class ReadWriteLockTests extends TestJPF {
    @Test(timeout = 500)
    public void testConcurrentReads() throws InterruptedException {
        Thread[] threads = new Thread[10];
        ReadWriteLock lock = new ReadWriteLock();

        for (int i = 0; i < 10; i++) {
            threads[i] = new Thread(() ->
            {
                try (ReadLock l = lock.lockRead()) {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    fail();
                }
            });
            threads[i].start();
        }

        for (int i = 0; i < 10; i++) {
            threads[i].join();
        }
    }

    @Test
    public void testConcurrentWrites() throws InterruptedException {
        if (verifyNoPropertyViolation()) {
            ReadWriteLock lock = new ReadWriteLock();
            SharedInteger s = new SharedInteger();
            s.i = 0;

            Thread t = new Thread(() ->
            {
                try (WriteLock l = lock.lockWrite()) {
                    s.i++;
                } catch (InterruptedException e) {
                    fail();
                }
            });

            t.start();
            try (WriteLock l = lock.lockWrite()) {
                s.i++;
            }
            t.join();
            assertEquals(s.i, 2);
        }
    }

    private class SharedInteger {
        private int i;
    }
}
