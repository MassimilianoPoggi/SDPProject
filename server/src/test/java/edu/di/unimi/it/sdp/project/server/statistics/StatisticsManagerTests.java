package edu.di.unimi.it.sdp.project.server.statistics;

import edu.di.unimi.it.sdp.project.model.ImmutableMeasurement;
import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class StatisticsManagerTests extends TestJPF {

    @Before
    public void clearStatistics() throws Exception {
        StatisticsManager.INSTANCE.clearAll();
    }

    @Test
    public void testAddOrderedGlobalMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
        StatisticsManager.INSTANCE.addGlobalMeasurement(m2);

        assertEquals(Arrays.asList(m2, m1), StatisticsManager.INSTANCE.getGlobalMeasurements(2));
    }

    @Test
    public void testAddOrderedEdgeMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        assertEquals(Arrays.asList(m2, m1), StatisticsManager.INSTANCE.getEdgeMeasurements(m1.getId(), 2));
    }

    @Test
    public void testAddOrderedLocalMeasurements() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID1", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID2", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        assertEquals(Arrays.asList(m2, m1), StatisticsManager.INSTANCE.getLocalMeasurements(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInexistentEdge() throws Exception {
        StatisticsManager.INSTANCE.getEdgeMeasurements("ID", 1);
    }

    @Test
    public void testTooManyEdgeMeasurementsAsked() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);

        assertEquals(1, StatisticsManager.INSTANCE.getEdgeMeasurements("ID", 10).size());
    }

    @Test
    public void testTooManyGlobalMeasurementsAsked() throws Exception {
        assertTrue(StatisticsManager.INSTANCE.getGlobalMeasurements(10).isEmpty());
    }

    @Test
    public void testTooManyLocalMeasurementsAsked() throws Exception {
        assertTrue(StatisticsManager.INSTANCE.getLocalMeasurements(10).isEmpty());
    }

    @Test
    public void testGlobalMean() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
        StatisticsManager.INSTANCE.addGlobalMeasurement(m2);

        assertEquals(7.5, StatisticsManager.INSTANCE.globalMean(2), 0.0001);
    }

    @Test
    public void testEdgeMean() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        assertEquals(7.5, StatisticsManager.INSTANCE.edgeMean(m1.getId(), 2), 0.0001);
    }

    @Test
    public void testGlobalStdDev() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
        StatisticsManager.INSTANCE.addGlobalMeasurement(m2);

        assertEquals(2.5, StatisticsManager.INSTANCE.globalStdDev(2), 0.0001);
    }

    @Test
    public void testEdgeStdDev() throws Exception {
        ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
        ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

        StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
        StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);

        assertEquals(2.5, StatisticsManager.INSTANCE.edgeStdDev(m1.getId(), 2), 0.0001);
    }

    @Test
    public void testConcurrentGlobalAdd() throws Exception {
        if (verifyNoPropertyViolation()) {
            ImmutableMeasurement m1 = new ImmutableMeasurement("Coordinator", "Type", 5.0, 5);
            ImmutableMeasurement m2 = new ImmutableMeasurement("Coordinator", "Type", 10.0, 10);

            new Thread(() -> {
                try {
                    StatisticsManager.INSTANCE.addGlobalMeasurement(m1);
                } catch (InterruptedException e) {
                    fail();
                }
            }).start();

            StatisticsManager.INSTANCE.addGlobalMeasurement(m2);
        }
    }

    @Test
    public void testConcurrentEdgeAdd() throws Exception {
        if (verifyNoPropertyViolation()) {
            ImmutableMeasurement m1 = new ImmutableMeasurement("ID", "Type", 5.0, 5);
            ImmutableMeasurement m2 = new ImmutableMeasurement("ID", "Type", 10.0, 10);

            new Thread(() -> {
                try {
                    StatisticsManager.INSTANCE.addEdgeMeasurement(m1.getId(), m1);
                } catch (InterruptedException e) {
                    fail();
                }
            }).start();

            StatisticsManager.INSTANCE.addEdgeMeasurement(m2.getId(), m2);
        }
    }
}
