package edu.di.unimi.it.sdp.project.server.utils;

import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Test;

import java.util.Arrays;

public class SynchronizedSortedListTests extends TestJPF {
    @Test
    public void testOrderedElements() throws Exception {
        SynchronizedSortedList<Integer> list = new SynchronizedSortedList<>();

        list.add(5);
        list.add(2);
        list.add(10);
        list.add(8);

        assertEquals(Arrays.asList(10, 8, 5, 2), list.subList(0, 4));
    }

    @Test
    public void testConcurrentAccess() {
        if (verifyNoPropertyViolation()) {
            SynchronizedSortedList<Integer> list = new SynchronizedSortedList<>();

            new Thread(() -> {
                try {
                    list.add(1);
                } catch (InterruptedException e) {
                    fail();
                }
            }).start();

            new Thread(() -> {
                try {
                    list.add(2);
                } catch (InterruptedException e) {
                    fail();
                }
            }).start();
        }
    }
}
