package edu.di.unimi.it.sdp.project.server.edges;

import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import gov.nasa.jpf.util.test.TestJPF;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

public class EdgeManagerTests extends TestJPF {

    @Before
    public void clearEdges() throws InterruptedException {
        EdgeManager.INSTANCE.clear();
    }

    @Test
    public void testAddEdge() throws Exception {
        Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID2", new GridPosition(50, 50), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);

        Collection<Edge> edges = EdgeManager.INSTANCE.add(e2);
        assertTrue(edges.size() == 2);
        assertTrue(edges.contains(e1));
        assertTrue(edges.contains(e2));
    }

    @Test(expected = IllegalStateException.class)
    public void testConflictingEdgeId() throws Exception {
        Edge e1 = new Edge("ID", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID", new GridPosition(50, 50), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);
        EdgeManager.INSTANCE.add(e2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConflictingEdgePosition() throws Exception {
        Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID2", new GridPosition(0, 0), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);
        EdgeManager.INSTANCE.add(e2);
    }

    @Test
    public void testRemoveEdge() throws Exception {
        Edge e = new Edge("ID", new GridPosition(0, 0), null, 0, 0);

        EdgeManager.INSTANCE.add(e);
        EdgeManager.INSTANCE.remove(e.getID());

        assertTrue(EdgeManager.INSTANCE.getEdges().isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveNonexistentEdge() throws Exception {
        EdgeManager.INSTANCE.remove("INVALID_ID");
    }

    @Test
    public void testFindNearestEdge() throws Exception {
        Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID2", new GridPosition(50, 50), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);
        EdgeManager.INSTANCE.add(e2);

        GridPosition position = new GridPosition(10, 10);
        assertEquals(e1, EdgeManager.INSTANCE.nearest(position));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testNearestWithoutEdges() throws Exception {
        EdgeManager.INSTANCE.nearest(new GridPosition(0, 0));
    }

    @Test
    public void testConcurrentAdds() throws Exception {
        if (verifyNoPropertyViolation()) {
            Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
            Edge e2 = new Edge("ID2", new GridPosition(50, 50), null, 0, 0);
            new Thread(() -> {
                try {
                    EdgeManager.INSTANCE.add(e1);
                } catch (InterruptedException e) {
                    fail();
                }
            }).start();

            EdgeManager.INSTANCE.add(e2);
        }
    }

    @Test
    public void testConcurrentRemoves() throws Exception {
        if (verifyNoPropertyViolation()) {
            Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
            Edge e2 = new Edge("ID2", new GridPosition(50, 50), null, 0, 0);

            EdgeManager.INSTANCE.add(e1);
            EdgeManager.INSTANCE.add(e2);

            new Thread(() -> {
                try {
                    EdgeManager.INSTANCE.remove("ID1");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();

            EdgeManager.INSTANCE.remove("ID2");
        }
    }
}
