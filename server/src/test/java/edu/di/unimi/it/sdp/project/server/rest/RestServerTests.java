package edu.di.unimi.it.sdp.project.server.rest;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.ext.RuntimeDelegate;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class RestServerTests {
    protected static final int SERVER_PORT = 8080;

    protected static HttpServer createServer(Class<?>... endpoints) throws IOException {
        URI uri = UriBuilder.fromUri("http://localhost/").port(SERVER_PORT).build();
        HttpServer server = HttpServer.create(new InetSocketAddress(uri.getPort()), 0);
        HttpHandler handler = RuntimeDelegate.getInstance()
                .createEndpoint(new ApplicationConfig(endpoints), HttpHandler.class);
        server.createContext(uri.getPath(), handler);
        return server;
    }

    protected static WebTarget getTargetToServer() {
        Client client = ClientBuilder.newClient();
        return client.target("http://localhost:" + SERVER_PORT);
    }

    private static class ApplicationConfig extends Application {
        Set<Class<?>> classes;

        ApplicationConfig(Class<?>... classes) {
            Set<Class<?>> cls = new HashSet<>(Arrays.asList(classes));
            this.classes = Collections.unmodifiableSet(cls);
        }

        @Override
        public Set<Class<?>> getClasses() {
            return classes;
        }
    }
}
