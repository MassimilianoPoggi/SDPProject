package edu.di.unimi.it.sdp.project.server.rest;

import com.sun.net.httpserver.HttpServer;
import edu.di.unimi.it.sdp.project.model.Edge;
import edu.di.unimi.it.sdp.project.model.GridPosition;
import edu.di.unimi.it.sdp.project.server.edges.EdgeManager;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class EdgesResourceTests extends RestServerTests {
    private static Client client;
    private static HttpServer server;
    private static WebTarget target;

    @BeforeClass
    public static void setUp() throws Exception {
        client = ClientBuilder.newClient();
        server = createServer(EdgesResource.class);
        target = client.target(UriBuilder.fromUri("http://localhost:" + SERVER_PORT));
        server.start();
    }

    @AfterClass
    public static void tearDown() {
        client.close();
        server.stop(0);
    }

    @Before
    public void clearEdges() throws Exception {
        EdgeManager.INSTANCE.clear();
    }

    @Test
    public void testAddEdge() throws Exception {
        Edge e = new Edge("ID", new GridPosition(0, 0), null, 0, 0);

        Response response =
                target.path("edges")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(Entity.json(e));

        assertEquals(Response.Status.CREATED, response.getStatusInfo().toEnum());
        assertEquals(Arrays.asList(e), EdgeManager.INSTANCE.getEdges());
    }

    @Test
    public void testConflictingEdgeId() throws Exception {
        Edge e1 = new Edge("ID", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID", new GridPosition(50, 50), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);

        Response response =
                target.path("edges")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(Entity.json(e2));

        assertEquals(Response.Status.CONFLICT, response.getStatusInfo().toEnum());
        assertEquals(EdgeManager.Conflict.EXISTING_ID, response.readEntity(EdgeManager.Conflict.class));
        assertEquals(Arrays.asList(e1), EdgeManager.INSTANCE.getEdges());
    }

    @Test
    public void testConflictingEdgePosition() throws Exception {
        Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID2", new GridPosition(1, 1), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);

        Response response =
                target.path("edges")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .put(Entity.json(e2));

        assertEquals(Response.Status.CONFLICT, response.getStatusInfo().toEnum());
        assertEquals(EdgeManager.Conflict.TOO_CLOSE, response.readEntity(EdgeManager.Conflict.class));
        assertEquals(Arrays.asList(e1), EdgeManager.INSTANCE.getEdges());
    }

    @Test
    public void testGetEdges() throws Exception {
        Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID2", new GridPosition(50, 50), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);
        EdgeManager.INSTANCE.add(e2);

        Response response =
                target.path("edges")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        List<Edge> edges = response.readEntity(new GenericType<List<Edge>>() {
        });
        assertEquals(2, edges.size());
        assertTrue(edges.contains(e1));
        assertTrue(edges.contains(e2));
    }

    @Test
    public void testRemoveEdge() throws Exception {
        Edge e = new Edge("ID", new GridPosition(0, 0), null, 0, 0);

        EdgeManager.INSTANCE.add(e);

        Response response =
                target.path("edges").path("ID")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .delete();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertTrue(EdgeManager.INSTANCE.getEdges().isEmpty());
    }

    @Test
    public void testRemoveNonexistentEdge() {
        Response response =
                target.path("channels").path("ID")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .delete();

        assertEquals(Response.Status.NOT_FOUND, response.getStatusInfo().toEnum());
    }

    @Test
    public void testNearest() throws Exception {
        Edge e1 = new Edge("ID1", new GridPosition(0, 0), null, 0, 0);
        Edge e2 = new Edge("ID2", new GridPosition(50, 50), null, 0, 0);

        EdgeManager.INSTANCE.add(e1);
        EdgeManager.INSTANCE.add(e2);

        Response response =
                target.path("edges").path("nearest").path("10x10")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.OK, response.getStatusInfo().toEnum());
        assertEquals(e1, response.readEntity(Edge.class));
    }

    @Test
    public void testNearestNoEdges() {
        Response response =
                target.path("channels").path("nearest").path("10x10")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.NOT_FOUND, response.getStatusInfo().toEnum());
    }

    @Test
    public void testNearestOutOfBounds() {
        Response response =
                target.path("edges").path("nearest").path("2000x2000")
                        .request(MediaType.APPLICATION_JSON_TYPE)
                        .get();

        assertEquals(Response.Status.BAD_REQUEST, response.getStatusInfo().toEnum());
    }
}
